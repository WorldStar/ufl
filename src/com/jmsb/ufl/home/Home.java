package com.jmsb.ufl.home;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.*;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.*;

import com.jmsb.ufl.R;
import com.jmsb.ufl.library.DBHelper;
import com.jmsb.ufl.library.NetworkConnectionDetector;
import com.jmsb.ufl.library.ReturnMessage;
import com.jmsb.ufl.news.BaseActivity;
import com.jmsb.ufl.news.NewsObject;
import com.jmsb.ufl.schedule.ScheduleObject;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Home extends Activity {
    ViewPager newsPager;
    int page = 0;
    final int MAX_PAGEVIEWER_SIZE = 5;
    DisplayImageOptions option;
    BaseActivity base;
    protected ImageLoader imageLoader = ImageLoader.getInstance();

    //hardcoded part for schedule
	/*private LayoutInflater mInflater;
	private Vector<RowData> data;
	RowData rd;

	
	static final String[] matchDate = new String[] { "3-OCT-2012",
			"7-OCT-2012", "21-OCT-2012", "24-OCT-2012", "31-OCT-2012"};

	static final String[] matchTime = new String[] { "18:45",
			"19:00", "14:00", "15:00", "14:00"};

	static final String[] localTeam = new String[] { "Man United", "Barcelona",
			"R Madrid", "R Betis", "Mallorca"};

	static final String[] visitorTeam = new String[] { "R Madrid", "R Madrid",
			"Valencia", "R Madrid", "R Madrid"};

	static final String[] localScore = new String[] { "1", "2", "1", "1", "0"};

	static final String[] visitorScore = new String[] { "2", "0", "1", "1", "0"};

	private Integer[] localImage = { R.drawable.manutd, R.drawable.bercelona,
			R.drawable.real_madrid, R.drawable.real_betis,
			R.drawable.mallorca};

	private Integer[] visitorImage = { R.drawable.real_madrid, R.drawable.real_madrid,
			R.drawable.valencia, R.drawable.real_madrid, R.drawable.real_madrid};*/

    /*String[] tvHeadline;
    String[] tvContent;
    String[] subImage;
    String[] tempCount;
    String[] webCont;
    String[] date;
    string temp1;
    int calc;*/
    String temp;
    DBHelper db;
    SharedPreferences mySP;
    SharedPreferences.Editor mySPEditor;
    View newsListLayout, newsDetailLayout, snewsDetailLayout;
    TextView head;
    boolean isNewsDetailShow = false;
    TextView newsDetailTitle, newsDetailDate, newsHeader, scheHeader;
    WebView newsDetailContent;
    ImageView newsDetailImage;
	Typeface font1,font2;
	LoadHome loadtask;
	boolean connctionflg = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);
        font1= Typeface.createFromAsset(getAssets(),
				"font/CALIBRIB.TTF");
		font2= Typeface.createFromAsset(getAssets(),
				"font/CALIBRI.TTF");
        db = new DBHelper(getContext());
        mySP = this.getSharedPreferences("myPref", MODE_PRIVATE);
        mySPEditor = mySP.edit();
        //Typeface tf = Typeface.createFromAsset(getAssets(), "font/Helvetica.otf");
        head = (TextView) findViewById(R.id.header);
        newsListLayout = findViewById(R.id.mainlayout);
        newsDetailLayout = findViewById(R.id.newsdetail);
        snewsDetailLayout = findViewById(R.id.snewsdetail);
        newsDetailTitle = (TextView)findViewById(R.id.newsdetailheadline);
        //newsDetailTitle.setTypeface(font1);
        newsHeader = (TextView)findViewById(R.id.title);
       // newsHeader.setTypeface(font1);
        scheHeader = (TextView)findViewById(R.id.schedule);
        //scheHeader.setTypeface(font1);
        newsDetailDate = (TextView)findViewById(R.id.newsdetailpublishdate);
        //newsDetailDate.setTypeface(font2);
        newsDetailContent = (WebView)findViewById(R.id.newsdetailwebview);
        newsDetailImage = (ImageView)findViewById(R.id.ivNewsImage);
//        option = new DisplayImageOptions.Builder()
//                .showStubImage(R.drawable.ic_stub)
//                .showImageForEmptyUri(R.drawable.ic_empty)
//                .showImageOnFail(R.drawable.ic_error)
//                .cacheInMemory()
//                .cacheOnDisc()
//                .displayer(new RoundedBitmapDisplayer(0))
//                .build();
        option = new DisplayImageOptions.Builder().showStubImage(R.drawable.ic_stub)
        		.showImageForEmptyUri(R.drawable.ic_empty1)
        		.showImageOnFail(R.drawable.ic_error)
        		.cacheInMemory()
        		.cacheOnDisc()
        		.displayer(new RoundedBitmapDisplayer(0))
        		.build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .defaultDisplayImageOptions(option)
                .build();

        ImageLoader.getInstance().init(config);

        loadtask = new LoadHome(getContext());
        loadtask.execute();

        //hardcoded part for schedule
		/*ListView lvSchedule = (ListView) findViewById(R.id.lvSchedule);
		mInflater = (LayoutInflater) getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		data = new Vector<RowData>();

		for (int i = 0; i < localTeam.length; i++) {
			try {
				rd = new RowData(i, localTeam[i]);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			data.add(rd);
		}
		CustomAdapter customadapter = new CustomAdapter(this,
				R.layout.schedule_list_row, R.id.title, data);
		lvSchedule.setAdapter(customadapter);
		lvSchedule.setTextFilterEnabled(true);*/
    }

    private Context getContext() {
        Context context;
        if (getParent() != null) context = getParent();
        else context = this;
        return context;
    }

    public class LoadHome extends AsyncTask<Void, Void, Boolean> {
        Context context;
        ProgressDialog progressDialog;
        List<NewsObject> newsList;
        List<ScheduleObject> scheduleList;

        public LoadHome(Context context){
            super();
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(context);
            progressDialog.setTitle(getString(R.string.LoadingDialog_Title));
            progressDialog.setMessage(getString(R.string.LoadingDialog_Content1));
            progressDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            if(NetworkConnectionDetector.isConnected(context)){
            	connctionflg = true;
                String timeStamp = "";
                String localTimeStamp = "";
                localTimeStamp = mySP.getString("LogoTimeStamp","");
                if(connctionflg)
                	timeStamp = getTimeStamp(getString(R.string.LogoTimeStampUrl));
                else return false;
                if(timeStamp.equalsIgnoreCase("NotFound")||!timeStamp.equalsIgnoreCase(localTimeStamp)){
                    mySPEditor.putString("LogoTimeStamp",timeStamp);
                    if(connctionflg)
                    	loadTeamLogo();
                    else return false;
              }

                localTimeStamp = mySP.getString("NewsTimeStamp","");
                if(connctionflg)
                	timeStamp = getTimeStamp(getString(R.string.NewsTimeStampUrl));
                else return false;
                if(timeStamp.equalsIgnoreCase("NotFound")||!timeStamp.equalsIgnoreCase(localTimeStamp)){
                    mySPEditor.putString("NewsTimeStamp",timeStamp);
                    if(connctionflg)
                    	loadNews();
                    else return false;
                }

                localTimeStamp = mySP.getString("ScheduleTimeStamp","");
                if(connctionflg)
                	timeStamp = getTimeStamp(getString(R.string.ScheduleTimeStampUrl));
                else return false;
                if(timeStamp.equalsIgnoreCase("NotFound")||!timeStamp.equalsIgnoreCase(localTimeStamp)){
                    mySPEditor.putString("ScheduleTimeStamp",timeStamp);
                    if(connctionflg)
                    	loadSchedule();
                    else return false;
                }
            }else{
                return false;
            }
            return true;
        }

        private String getTimeStamp(String timeStampUrl) {
            try{
                NodeList newsTimeStampNode = getContentFromXML(timeStampUrl,"ts");
                Element nodeElement = (Element) newsTimeStampNode.item(0);
                newsTimeStampNode = nodeElement.getChildNodes();
                return ((Node)newsTimeStampNode.item(0)).getNodeValue();
            }catch (Exception e){
                Log.e("Error retrieving timestamp ", e.toString());
                return "NotFound";
            }
        }

        private void loadTeamLogo() {
            NodeList teamLogoNodeList = getContentFromXML(getString(R.string.TeamLogoUrl), "team");
            if(teamLogoNodeList != null){
                downloadTeamLogo(teamLogoNodeList);
            }
        }

        private void downloadTeamLogo(NodeList teamLogoNodeList) {
            Node node = null;
            String teamLogoImageUrl = null;
            String teamID = null;
            Bitmap downloadedTeamLogo = null;
            for(int i=0;i<teamLogoNodeList.getLength();i++){
                node = teamLogoNodeList.item(i);
                teamID = ((Element) node).getAttribute("id");
                
                teamLogoImageUrl = ((Element) node).getAttribute("img");
                try {
                	
                    teamLogoImageUrl = teamLogoImageUrl.replace(" ","%20");
                    //String splitString[] = teamLogoImageUrl.split("leaguemanager/");
                    //String teamImageID = URLEncoder.encode(splitString[1],"UTF-8");
                    //teamLogoImageUrl = splitString[0]+"leaguemanager/"+teamImageID;
                    InputStream in = new java.net.URL(teamLogoImageUrl).openStream();
                    downloadedTeamLogo = BitmapFactory.decodeStream(in);
                    saveImageToLocal(downloadedTeamLogo, teamID);
                } catch (Exception e) {
                    //Log.e("Error", e.getMessage());
                    e.printStackTrace();
                }
            }
        }

        private void saveImageToLocal(Bitmap downloadedTeamLogo, String teamID) {
            File myDir=new File(Environment.getExternalStorageDirectory().toString()+"/TaiwanIntercity");
        	String s = Environment.getExternalStorageDirectory().toString()+"/TaiwanIntercity";
            
        	if(!myDir.exists())
            	myDir.mkdirs();
        	File myDir2=new File(getApplicationContext().getFilesDir().getAbsolutePath()+"/TaiwanIntercity");
        	if(!myDir.exists()){
        		myDir = myDir2;
        	}

            String fname = teamID+".png";
            File file = new File (myDir, fname);
            if (file.exists ()) file.delete ();
            try {
                FileOutputStream out = new FileOutputStream(file);
                downloadedTeamLogo.compress(Bitmap.CompressFormat.PNG, 90, out);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void loadNews() {
            //if timestamp smaller than local timestamp, do this
            NodeList newsNodeList = getContentFromXML(getString(R.string.NewsUrl), "post");
            if(newsNodeList != null){
                db.dropNewsTable();
                newsList = convertNewsNodeToObjectList(newsNodeList);
            }
            /*try {

                URL url = new URL("http://fantasy4all.com/rmdemo/?feed=rss2");
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                Document doc = db.parse(new InputSource(url.openStream()));
                doc.getDocumentElement().normalize();

                NodeList nodeList = doc.getElementsByTagName("item");

                //Assign textview array lenght by arraylist size
                tvHeadline = new String[nodeList.getLength()];
                tvContent = new String[nodeList.getLength()];
                tempCount = new String[nodeList.getLength()];
                subImage = new String[nodeList.getLength()];
                webCont = new String[nodeList.getLength()];
                date = new String[nodeList.getLength()];
                calc = tvHeadline.length;

                for (int i = 0; i < nodeList.getLength(); i++) {

                    Node node = nodeList.item(i);

                    Element fstElmnt = (Element) node;
                    NodeList nameList = fstElmnt.getElementsByTagName("title");
                    Element nameElement = (Element) nameList.item(0);
                    nameList = nameElement.getChildNodes();
                    tvHeadline[i] = ((Node)nameList.item(0)).getNodeValue();

                    NodeList websiteList = fstElmnt.getElementsByTagName("description");
                    Element websiteElement = (Element) websiteList.item(0);
                    websiteList = websiteElement.getChildNodes();
                    tvContent[i] = ((Node) websiteList.item(0)).getNodeValue();

                    NodeList webContent = fstElmnt.getElementsByTagName("content:encoded");
                    Element webContentElement = (Element) webContent.item(0);
                    webContent = webContentElement.getChildNodes();
                    webCont[i] = ((Node) webContent.item(0)).getNodeValue();

                    NodeList dateList = fstElmnt.getElementsByTagName("pubDate");
                    Element dateElement = (Element) dateList.item(0);
                    dateList = dateElement.getChildNodes();
                    date[i] = ((Node) dateList.item(0)).getNodeValue();

                    temp = tvContent[i];
                    tempCount = temp.split("\"");
                    temp1 = tempCount[1];
                    String noHTMLString = temp.replaceAll("\\<.*?>","");
                    tvContent[i] = noHTMLString;
                    subImage[i] = temp1;
                }

            } catch (Exception e) {
                System.out.println("XML Pasing Excpetion = " + e);
            }*/
        }

        private void displayNews() {
            newsPager = (ViewPager) findViewById(R.id.newsPager);
            List<NewsObject> newsObjectList = db.getNews();
            if(newsObjectList.size()!=0){
                MyPagerAdapter adapter = new MyPagerAdapter(newsObjectList);
                newsPager.setAdapter(adapter);
                newsPager.setCurrentItem(0);
                newsPager.postDelayed(run, 3000);
            }
        }

        private void loadSchedule() {
            NodeList teamNodeList = getContentFromXML(getString(R.string.ScheduleUrl), "match");
            if(teamNodeList != null){
                db.dropScheduleTable();
                scheduleList = convertNodeToObjectList(teamNodeList);
                mySPEditor.putInt("match day",getMatchDay(scheduleList)).commit();
            }
        }

        private void displaySchedule(){
            List<ScheduleObject> scheduleObjectList = db.getSchedule();
            showSchedule(context, scheduleObjectList);
        }

        private void showSchedule(Context context, List<ScheduleObject> list) {
            //int match_day = getMatchDay(list);
            List<ScheduleObject> filteredList = new ArrayList<ScheduleObject>();
            filteredList = filterSchedule(list);
            if(filteredList!=null){
                ListView scheduleListView = (ListView)findViewById(R.id.lvSchedule);
                ScheduleAdapter scheduleAdapter = new ScheduleAdapter(context, 0, filteredList){
                    public boolean isEnabled(int position)
                    {
                        return false;
                    }
                };
                scheduleListView.setAdapter(scheduleAdapter);
            }
        }

        private List<ScheduleObject> filterSchedule(List<ScheduleObject> list) {
        	 List<ScheduleObject> filteredList = new ArrayList<ScheduleObject>();
             ScheduleObject scheduleObject;
             int position = 0;
             String months;
             String match_date = "";
             Calendar calendar = Calendar.getInstance();
             int year = calendar.get(Calendar.YEAR);
             int month = calendar.get(Calendar.MONTH) + 1;
             int day = calendar.get(Calendar.DAY_OF_MONTH);
             SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
             SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");
             try {
                 //DateFormat today1 = simpleDateFormat.getDateInstance();
                 String todayDate = ""+year+"-"+month+"-"+day;
                 Date today = simpleDateFormat.parse(todayDate);
                 int count = 1;
                 int count1 =0;
                 String score = "";
                 Date matchDate;
                 Date previousDate = null;
                 Date nextDate = null;
                 Date tempDate = null;
                 Date beforeDate = null;
                 //Date newDate = new Date (today.getTime()+604800000L);
                 for(int i=0;i<list.size();i++){
                 	scheduleObject = list.get(i);
                 	beforeDate = simpleDateFormat.parse(scheduleObject.getDate());
                 	if(beforeDate.before(today)){
                 		tempDate = beforeDate;
                 		count1++;
                 	}
                 	else{
                 		break;
                 	}
                 }
                 
                 for(int i=(count1-1); i<list.size();i--){
                 	scheduleObject = list.get(i);
                 	beforeDate = simpleDateFormat.parse(scheduleObject.getDate());
                 	if(beforeDate.equals(tempDate)){
                 		if(match_date.length()==0 || match_date.equalsIgnoreCase(scheduleObject.getDate())){
                 			scheduleObject.setMatchNumber(position);
                 			position = position+1;
                 		}else{
                 			position = 0;
                 			scheduleObject.setMatchNumber(position);
                 			position = position+1;
                 			}
                 		match_date = scheduleObject.getDate();
                 		scheduleObject.setdate(sdf.format(beforeDate));
                 		filteredList.add(scheduleObject);
                 	}
                 	else{
                 		break;
                 	}
                 }
                 
                 for(int i=(count1);i<list.size();i++){
                     scheduleObject = list.get(i);
                     nextDate = simpleDateFormat.parse(scheduleObject.getDate());
                     if(count<=7){
                     if((nextDate.after(today)&&nextDate.after(tempDate))||count>7){
                     	previousDate = tempDate;
                     	count++;
                     	}
                     else{
                     	if(i!=0 && !nextDate.equals(tempDate)){
                     		previousDate = tempDate; 
                     		}
                     	}
                     tempDate = nextDate;
                     }
                     else{
                     	break;
                     }
                 }   
                 
                
                 for(int i=(count1);i<list.size() ;i++){
                     scheduleObject = list.get(i);
                     matchDate = simpleDateFormat.parse(scheduleObject.getDate());
                     	if((matchDate.after(today)||matchDate.equals(today))&&!(matchDate.after(tempDate))){
                     		if(match_date.length()==0 || match_date.equalsIgnoreCase(scheduleObject.getDate())){
                     			scheduleObject.setMatchNumber(position);
                     			position = position+1;
                     		}else{
                     			position = 0;
                     			scheduleObject.setMatchNumber(position);
                     			position = position+1;
                     			}
                     		match_date = scheduleObject.getDate();
                     		scheduleObject.setdate(sdf.format(matchDate));
                     		filteredList.add(scheduleObject);
                     		}
                     	}
             }catch (Exception e){
                 e.printStackTrace();
             }
             return filteredList;
         }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            progressDialog.dismiss();
            if(aBoolean == true){
                try{
                    displayNews();
                    displaySchedule();
                    mySPEditor.putBoolean("isFirstTime",false).commit();
                }catch (Exception e){
                    ReturnMessage.showAlertDialog(context, "Unable to load", "Unable to load Home, please try again later.");
                }
            }else{
                if(mySP.getBoolean("isFirstTime",true)==true){
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setMessage("Unable to load").setCancelable(false);
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                        	dialog.dismiss();
                        }
                    });
                    builder.setPositiveButton(getString(R.string.InternetNotFound_DialogPositiveButton), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            new LoadHome(context).execute();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle(getString(R.string.InternetNotFound_DialogTitle));
                    builder.setMessage(getString(R.string.InternetNotFound_DialogContent)).setCancelable(false);
                    builder.setNegativeButton(getString(R.string.InternetNotFound_DialogNegativeButton), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            displayNews();
                            displaySchedule();
                        }
                    });
                    builder.setPositiveButton(getString(R.string.InternetNotFound_DialogPositiveButton), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            new LoadHome(context).execute();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            }
        }
    }

    private int getMatchDay(List<ScheduleObject> list) {
        int match_day = 1;
        ScheduleObject tempScheduleObject;
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH)+1;
        int day = c.get(Calendar.DAY_OF_MONTH);
        try{
            Date date_todayDate = new SimpleDateFormat("yyyy-MM-dd").parse(year+"-"+month+"-"+day);
            for(int i=0;i<list.size();i++){
                tempScheduleObject = list.get(i);
                String string_matchDate = tempScheduleObject.getDate();
                Date date_matchDate = new SimpleDateFormat("yyyy-MM-dd").parse(string_matchDate);
                if(date_matchDate.after(date_todayDate)){
                    match_day = tempScheduleObject.getMatchDay()-1;
                    if(date_matchDate.equals(date_todayDate))
                        match_day = match_day -1;
                    break;
                }
            }
        } catch (java.text.ParseException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return match_day;
    }

    private class ScheduleAdapter extends ArrayAdapter<ScheduleObject> {
        Context context;
        List<ScheduleObject> list;
        String matchDate = "";
        int match_day;

        public class ScheduleHolder{
            RelativeLayout dateBanner;
            RelativeLayout rowContent;
            TextView date;
            TextView time;
            TextView homeTeam;
            TextView homeScore;
            TextView awayTeam;
            TextView awayScore;
            ImageView homeLogo;
            ImageView awayLogo;
        }

        public ScheduleAdapter(Context context, int match_day, List<ScheduleObject> list) {
            super(context, match_day, list);
            this.context = context;
            this.list = list;
            this.match_day = match_day;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ScheduleHolder holder;
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            //Typeface tf = Typeface.createFromAsset(getAssets(), "font/Helvetica.otf");
            if(convertView == null){
                convertView = inflater.inflate(R.layout.schedule_list_row, parent, false);
                holder = new ScheduleHolder();
                holder.date = (TextView)convertView.findViewById(R.id.tvDate);
                //holder.date.setTypeface(font1);
                holder.dateBanner = (RelativeLayout)convertView.findViewById(R.id.rlDateBanner);
                holder.rowContent = (RelativeLayout)convertView.findViewById(R.id.row_content);
                holder.time = (TextView)convertView.findViewById(R.id.tvTime);
                //holder.time.setTypeface(font2, 13);
                holder.homeTeam = (TextView)convertView.findViewById(R.id.tvLocal_team);
                //holder.homeTeam.setTypeface(font2, 13);
                holder.homeScore = (TextView)convertView.findViewById(R.id.tvLocal_score);
                //holder.homeScore.setTypeface(font2, 13);
                holder.awayTeam = (TextView)convertView.findViewById(R.id.tvVisitor_team);
                //holder.awayTeam.setTypeface(font2, 13);
                holder.awayScore = (TextView)convertView.findViewById(R.id.tvVisitor_score);
                //holder.awayScore.setTypeface(font2, 13);
                holder.homeLogo = (ImageView)convertView.findViewById(R.id.ivLocalImage);
                holder.awayLogo = (ImageView)convertView.findViewById(R.id.ivVisitorImage);
                convertView.setTag(holder);
            }
            else
                holder = (ScheduleHolder)convertView.getTag();
            ScheduleObject scheduleObject = list.get(position);
            if(scheduleObject.getMatchNumber() != 0){
                holder.dateBanner.setVisibility(View.GONE);
            }else{
                holder.dateBanner.setVisibility(View.VISIBLE);
                holder.date.setText(scheduleObject.getDate());
            }
            holder.time.setText(scheduleObject.getTime());
            holder.homeTeam.setText(scheduleObject.getHomeTeam());
            holder.homeScore.setText(scheduleObject.getHomeScore());
            holder.awayTeam.setText(scheduleObject.getAwayTeam());
            holder.awayScore.setText(scheduleObject.getAwayScore());
            File imgFile = new File(Environment.getExternalStorageDirectory().toString()+"/TaiwanIntercity/"+scheduleObject.getHomeTeamID()+".png");
            if(imgFile.exists()){
                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                holder.homeLogo.setImageBitmap(myBitmap);
            }else{

            	imgFile=new File(getApplicationContext().getFilesDir().getAbsolutePath()+"/TaiwanIntercity/"+scheduleObject.getHomeTeamID()+".png");
            	if(imgFile.exists()){
                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    holder.homeLogo.setImageBitmap(myBitmap);
                }else{
                	holder.homeLogo.setImageResource(R.drawable.rihanna);
                }
            }
            imgFile = new File(Environment.getExternalStorageDirectory().toString()+"/TaiwanIntercity/"+scheduleObject.getAwayTeamID()+".png");
            if(imgFile.exists()){
                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                holder.awayLogo.setImageBitmap(myBitmap);
            }else{
            	imgFile=new File(getApplicationContext().getFilesDir().getAbsolutePath()+"/TaiwanIntercity/"+scheduleObject.getAwayTeamID()+".png");
            	if(imgFile.exists()){
                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    holder.awayLogo.setImageBitmap(myBitmap);
                }else{
                	holder.awayLogo.setImageResource(R.drawable.rihanna);
                }
            }
            matchDate = scheduleObject.getDate();
            return convertView;
        }
    }

    private NodeList getContentFromXML(String urlString, String tagName) {
        NodeList nodeList = null;
        try {
            URL url = new URL(urlString);
            URLConnection con = url.openConnection();
            con.setReadTimeout(30000);
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(con.getInputStream());
            doc.getDocumentElement().normalize();
            nodeList = doc.getElementsByTagName(tagName);
        }catch (SocketTimeoutException ex){
        	connctionflg = false;
        	Home.this.runOnUiThread(new Runnable() {
				@Override
				public void run() {
	            	//loadtask.cancel(true);
					//TimeOutProcessing();		
				}
			});	
        	
        }catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return nodeList;
    }
    private void TimeOutProcessing(){
    	AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Unable to load").setCancelable(false);
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                loadtask = new LoadHome(getContext());
                loadtask.execute();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }
    private List<NewsObject> convertNewsNodeToObjectList(NodeList nodeList) {
        Node node = null;
        List<NewsObject> newsList= new ArrayList<NewsObject>();
        NewsObject news;
        int newsListSize = nodeList.getLength();
        for(int i=0; i<newsListSize; i++){
            node = nodeList.item(i);
            news = new NewsObject();
            news.setTitle(((Element) node).getAttribute("title"));
            news.setDate(((Element) node).getAttribute("publish_date"));
            //news.setContent(((Element) node).getAttribute("content"));
            Element fstElmnt = (Element) node;
            NodeList webContent = fstElmnt.getElementsByTagName("content");
            Element webContentElement = (Element) webContent.item(0);
            webContent = webContentElement.getChildNodes();
            try{
                news.setContent(((Node) webContent.item(0)).getNodeValue());
                temp = news.getContent();
                news.setListContent(temp.replaceAll("\\<.*?>","").replace("\n","").replace("\r",""));
            }catch (Exception e){
                news.setContent("");
            }
            news.setImageUrl(((Element)node).getAttribute("image"));
            newsList.add(news);
            db.setNews(news);
            //db.setSchedule(schedule);
        }
        return newsList;
    }

    private List<ScheduleObject> convertNodeToObjectList(NodeList nodeList) {
        Node node = null;
        List<ScheduleObject> scheduleList= new ArrayList<ScheduleObject>();
        ScheduleObject schedule;
        String matchDate = "";
        int position = 0;
        int nodeListSize = nodeList.getLength();
        for(int i=0; i<nodeListSize; i++){
            node = nodeList.item(i);
            schedule = new ScheduleObject();
            schedule.setHomeTeamAndAwayTeam(((Element) node).getAttribute("title"));
            schedule.setHomeScoreAndAwayScore(((Element) node).getAttribute("score"));
            schedule.setLocation(((Element) node).getAttribute("location"));
            schedule.setDateAndTime(((Element)node).getAttribute("date"));
            schedule.setDateAndTime1(((Element)node).getAttribute("time"));
            schedule.setHomeAndAwayTeamID(((Element)node).getAttribute("ids"));
            schedule.setMatchDay(Integer.parseInt(((Element)node).getAttribute("match_day")));
            if(matchDate.equalsIgnoreCase("") || !(matchDate.equalsIgnoreCase(schedule.getDate()))){
                position = 0;
                schedule.setMatchNumber(position);
            }else{
                position = position +1;
                schedule.setMatchNumber(position);
            }
            matchDate = schedule.getDate();
            scheduleList.add(schedule);
            db.setSchedule(schedule);
        }
        return scheduleList;
    }

    // For news banner, i used view pager
    private class MyPagerAdapter extends PagerAdapter {
        private View view;
        List<NewsObject> newsList;
        NewsObject newsObject;

        public MyPagerAdapter(List<NewsObject> newsList) {
            super();
            this.newsList = newsList;
        }

        public int getCount() {
            return MAX_PAGEVIEWER_SIZE;
        }

        @Override
        public void destroyItem(View arg0, int arg1, Object arg2) {
            ((ViewPager) arg0).removeView((View) arg2);
        }

        @Override
        public void finishUpdate(View arg0) {
            // TODO Auto-generated method stub
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == ((View) arg1);
        }

        @Override
        public void restoreState(Parcelable arg0, ClassLoader arg1) {
            // TODO Auto-generated method stub
        }

        @Override
        public Parcelable saveState() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public void startUpdate(View arg0) {
            // TODO Auto-generated method stub
        }

        public Object instantiateItem(View collection, final int position) {
            LayoutInflater inflater = (LayoutInflater) collection.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            view = inflater.inflate(R.layout.newslist, null);
            //Typeface tf = Typeface.createFromAsset(getAssets(), "font/Helvetica.otf"); 
           // Typeface tf1 = Typeface.createFromAsset(getAssets(), "font/HelveticaBold.ttf");
            ((ViewPager) collection).addView(view, 0);
            ImageView ivNewsImage1 = (ImageView) view
                    .findViewById(R.id.ivNewsImage);
            TextView tvNewsHeadline1 = (TextView) view
                    .findViewById(R.id.tvNewsHeadline);
            //tvNewsHeadline1.setTypeface(font1,13);
            //tvNewsHeadline1.setTextSize(12);
            TextView tvNewsContent1 = (TextView) view
                    .findViewById(R.id.tvNewsContent);
            //tvNewsContent1.setTypeface(font2);
            tvNewsContent1.setMaxLines(2);
            Button detail1 = (Button) view.findViewById(R.id.btReadMore);
            //etail1.setTypeface(font1);

            newsObject = newsList.get(position);
            try{
                imageLoader.displayImage(newsObject.getImageUrl(), ivNewsImage1);
            }catch (Exception e){
            }
            tvNewsHeadline1.setText(newsObject.getTitle());
            tvNewsContent1.setText(newsObject.getListContent());

            detail1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    // TODO Auto-generated method stub
                    /*Intent intent = new Intent(getParent(), NewsDetail1.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    Bundle b = new Bundle();
                    NewsObject newsObjectToPass = newsList.get(position);
                    b.putString("title", newsObjectToPass.getTitle());
                    b.putString("date", newsObjectToPass.getDate());
                    b.putString("image", newsObjectToPass.getImageUrl());
                    b.putString("content", newsObjectToPass.getContent());
                    intent.putExtras(b);
                    startActivity(intent);*/
                    NewsObject newsObject1 = newsList.get(position);
                    showNewsDetail(newsObject1);
                    
                }
            });
				/*
			case 1:
				view = inflater.inflate(R.layout.newslist, null);
				((ViewPager) collection).addView(view, 0);
				ImageView ivNewsImage2 = (ImageView) view
						.findViewById(R.id.ivNewsImage);
				TextView tvNewsHeadline2 = (TextView) view
						.findViewById(R.id.tvNewsHeadline);
				TextView tvNewsContent2 = (TextView) view
						.findViewById(R.id.tvNewsContent);
				Button detail2 = (Button) view.findViewById(R.id.btReadMore);

				ivNewsImage2.setImageDrawable(getResources().getDrawable(
						R.drawable.rmnewimage2));
				tvNewsHeadline2
						.setText("La Liga Team of the Week: Messi & Ronaldo joined by Fabregas & Higuain");
				tvNewsContent2
						.setText("It was a routine weekend for Barcelona and Real...");

				detail2.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						Intent intent = new Intent(getParent(),
								NewsDetail2.class);
						intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						TabGroupActivity parentActivity = (TabGroupActivity) getParent();
						parentActivity
								.startChildActivity("NewsDetail2", intent);
					}
				});
				break;
			case 2:
				view = inflater.inflate(R.layout.newslist, null);
				((ViewPager) collection).addView(view, 0);
				ImageView ivNewsImage3 = (ImageView) view
						.findViewById(R.id.ivNewsImage);
				TextView tvNewsHeadline3 = (TextView) view
						.findViewById(R.id.tvNewsHeadline);
				TextView tvNewsContent3 = (TextView) view
						.findViewById(R.id.tvNewsContent);
				Button detail3 = (Button) view.findViewById(R.id.btReadMore);
				ivNewsImage3.setImageDrawable(getResources().getDrawable(
						R.drawable.rmnewimage3));
				tvNewsHeadline3
						.setText("United?s German transfer target happy in La Liga");
				tvNewsContent3
						.setText("Manchester United transfer target Mesut Ozil insists...");

				detail3.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						Intent intent = new Intent(getParent(),
								NewsDetail3.class);
						intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						TabGroupActivity parentActivity = (TabGroupActivity) getParent();
						parentActivity
								.startChildActivity("NewsDetail3", intent);
					}
				});
				break;
			case 3:
				view = inflater.inflate(R.layout.newslist, null);
				((ViewPager) collection).addView(view, 0);
				ImageView ivNewsImage4 = (ImageView) view
						.findViewById(R.id.ivNewsImage);
				TextView tvNewsHeadline4 = (TextView) view
						.findViewById(R.id.tvNewsHeadline);
				TextView tvNewsContent4 = (TextView) view
						.findViewById(R.id.tvNewsContent);
				Button detail4 = (Button) view.findViewById(R.id.btReadMore);

				ivNewsImage4.setImageDrawable(getResources().getDrawable(
						R.drawable.rmnewimage4));
				tvNewsHeadline4
						.setText("?50m Tottenham star is Real Madrid?s top summer target");
				tvNewsContent4
						.setText("Tottenham star Gareth Bale is reportedly Real...");

				detail4.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						Intent intent = new Intent(getParent(),
								NewsDetail4.class);
						intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						TabGroupActivity parentActivity = (TabGroupActivity) getParent();
						parentActivity
								.startChildActivity("NewsDetail4", intent);
					}
				});
				break;

			case 4:
				view = inflater.inflate(R.layout.newslist, null);
				((ViewPager) collection).addView(view, 0);
				ImageView ivNewsImage5 = (ImageView) view
						.findViewById(R.id.ivNewsImage);
				TextView tvNewsHeadline5 = (TextView) view
						.findViewById(R.id.tvNewsHeadline);
				TextView tvNewsContent5 = (TextView) view
						.findViewById(R.id.tvNewsContent);
				Button detail5 = (Button) view.findViewById(R.id.btReadMore);

				ivNewsImage5.setImageDrawable(getResources().getDrawable(
						R.drawable.rmnewimage5));
				tvNewsHeadline5
						.setText("Spanish La Liga Team of the Week");
				tvNewsContent5
						.setText("Barcelona romped to a 5-0 win over a resilient Rayo...");

				detail5.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						Intent intent = new Intent(getParent(),
								NewsDetail5.class);
						intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						TabGroupActivity parentActivity = (TabGroupActivity) getParent();
						parentActivity
								.startChildActivity("NewsDetail5", intent);
					}
				});
				break;
			}*/
            return view;
        }
    }

    private void showNewsDetail(NewsObject newsObject) {
        //Typeface tf = Typeface.createFromAsset(getAssets(), "font/Helvetica.otf");
    	newsListLayout.setVisibility(View.GONE);
        newsDetailLayout.setVisibility(View.VISIBLE);
        snewsDetailLayout.setVisibility(View.VISIBLE);
        head.setText("Latest News");
        //head.setTypeface(tf);
        isNewsDetailShow = true;
        newsDetailTitle.setText(newsObject.getTitle());
        //newsDetailTitle.setTypeface(tf, Typeface.BOLD);
        newsDetailDate.setText(newsObject.getDate());
        newsDetailContent.getSettings().setJavaScriptEnabled(true);
        //newsDetailContent.getSettings().setUseWideViewPort( true  );
        newsDetailContent.getSettings().setBuiltInZoomControls(true);
        newsDetailContent.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
        newsDetailContent.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        newsDetailContent.loadDataWithBaseURL(null, newsObject.getContent(), "text/html", "UTF-8", null);
        imageLoader.displayImage(newsObject.getImageUrl(), newsDetailImage);
        newsDetailContent.requestFocus();
        snewsDetailLayout.scrollTo(0, 0);
    }

    // auto scroll the view pager
    Runnable run = new Runnable() {
        @Override
        public void run() {
            // TODO Auto-generated method stub
            page = newsPager.getCurrentItem();
            Log.d("current page", page + "");
            if (page < MAX_PAGEVIEWER_SIZE-1) {
                page++;
            } else {
                page = 0;
            }
            Log.d("page after", page + "");
            newsPager.setCurrentItem(page, true);
            newsPager.postDelayed(run, 6000);
        }
    };

    /* hardcoded part // For schedule part
    private class RowData {
        protected int mId;
        protected String mTitle;

        RowData(int id, String title) {
            mId = id;
            mTitle = title;
        }

        @Override
        public String toString() {
            return mId + " " + mTitle;
        }
    }

    private class CustomAdapter extends ArrayAdapter<RowData> {
        public CustomAdapter(Context context, int resource,
                int textViewResourceId, List<RowData> objects) {
            super(context, resource, textViewResourceId, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            RelativeLayout rlDateBanner = null;
            TextView tvMatchDate = null;
            TextView tvMatchTime = null;
            TextView tvLocalScore = null;
            ImageView ivLocalImage = null;
            ImageView ivVisitorImage = null;
            TextView tvVisitorScore = null;
            TextView tvLocalTeam = null;
            TextView tvVisitorTeam = null;

            RowData rowData = getItem(position);
            if (null == convertView) {
                convertView = mInflater.inflate(R.layout.schedule_list_row,
                        null);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            }
            holder = (ViewHolder) convertView.getTag();

            rlDateBanner = holder.getDateBanner();
            if (rowData.mId != 0) {
                if (matchDate[rowData.mId]
                        .contentEquals(matchDate[rowData.mId - 1])) {
                    rlDateBanner.setVisibility(View.GONE);
                }
            }

            tvMatchDate = holder.getMatchDate();
            tvMatchDate.setText(matchDate[rowData.mId]);

            tvMatchTime = holder.getMatchTime();
            tvMatchTime.setText(matchTime[rowData.mId]);

            tvLocalScore = holder.getLocalScore();
            tvLocalScore.setText(localScore[rowData.mId]);
            tvLocalScore.setVisibility(View.INVISIBLE);

            ivLocalImage = holder.getLocalImage();
            ivLocalImage.setImageResource(localImage[rowData.mId]);

            ivVisitorImage = holder.getVisitorImage();
            ivVisitorImage.setImageResource(visitorImage[rowData.mId]);

            tvVisitorScore = holder.getVisitorScore();
            tvVisitorScore.setText(visitorScore[rowData.mId]);
            tvVisitorScore.setVisibility(View.INVISIBLE);

            tvLocalTeam = holder.getLocalTeam();
            tvLocalTeam.setText(localTeam[rowData.mId]);

            tvVisitorTeam = holder.getVisitorTeam();
            tvVisitorTeam.setText(visitorTeam[rowData.mId]);
            return convertView;
        }

        private class ViewHolder {
            private View mRow;
            private RelativeLayout rlDateBanner;
            private TextView tvMatchDate;
            private TextView tvMatchTime;
            private TextView tvLocalScore;
            private ImageView ivLocalImage;
            private ImageView ivVisitorImage;
            private TextView tvVisitorScore;
            private TextView tvLocalTeam;
            private TextView tvVisitorTeam;

            public ViewHolder(View row) {
                mRow = row;
            }

            public RelativeLayout getDateBanner() {
                if (rlDateBanner == null) {
                    rlDateBanner = (RelativeLayout) mRow
                            .findViewById(R.id.rlDateBanner);
                }
                return rlDateBanner;
            }

            public TextView getMatchDate() {
                if (tvMatchDate == null) {
                    tvMatchDate = (TextView) mRow.findViewById(R.id.tvDate);
                }
                return tvMatchDate;
            }

            public TextView getMatchTime() {
                if (tvMatchTime == null) {
                    tvMatchTime = (TextView) mRow.findViewById(R.id.tvTime);
                }
                return tvMatchTime;
            }

            public TextView getLocalScore() {
                if (tvLocalScore == null) {
                    tvLocalScore = (TextView) mRow
                            .findViewById(R.id.tvLocal_score);
                }
                return tvLocalScore;
            }

            public ImageView getLocalImage() {
                if (ivLocalImage == null) {
                    ivLocalImage = (ImageView) mRow
                            .findViewById(R.id.ivLocalImage);
                }
                return ivLocalImage;
            }

            public ImageView getVisitorImage() {
                if (ivVisitorImage == null) {
                    ivVisitorImage = (ImageView) mRow
                            .findViewById(R.id.ivVisitorImage);
                }
                return ivVisitorImage;
            }

            public TextView getVisitorScore() {
                if (tvVisitorScore == null) {
                    tvVisitorScore = (TextView) mRow
                            .findViewById(R.id.tvVisitor_score);
                }
                return tvVisitorScore;
            }

            public TextView getLocalTeam() {
                if (tvLocalTeam == null) {
                    tvLocalTeam = (TextView) mRow
                            .findViewById(R.id.tvLocal_team);
                }
                return tvLocalTeam;
            }

            public TextView getVisitorTeam() {
                if (tvVisitorTeam == null) {
                    tvVisitorTeam = (TextView) mRow
                            .findViewById(R.id.tvVisitor_team);
                }
                return tvVisitorTeam;
            }
        }
    } */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.option_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_refresh:
                new LoadHome(getContext()).execute();
                break;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        if(isNewsDetailShow){
            newsListLayout.setVisibility(View.VISIBLE);
            newsDetailLayout.setVisibility(View.GONE);
            snewsDetailLayout.setVisibility(View.GONE);
            isNewsDetailShow = false;
        }
        else{
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setMessage(getString(R.string.ExitDialog_Content)).setCancelable(false);
            builder.setNegativeButton(getString(R.string.ExitDialog_NoButton), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            });
            builder.setPositiveButton(getString(R.string.ExitDialog_YesButton), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }
}