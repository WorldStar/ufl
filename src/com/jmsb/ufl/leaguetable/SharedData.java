package com.jmsb.ufl.leaguetable;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedData {
	protected SharedPreferences shareObject;
	   protected SharedPreferences.Editor editor;
	   protected String FIELNAME = "storexml";
	   protected String XMLDATA = "xmlString";
	   public SharedData(Context _context){
		   shareObject = _context.getSharedPreferences(FIELNAME, Context.MODE_PRIVATE);
		   editor = shareObject.edit();
	   }
	   public void setStore(String xml){
		   editor.putString(XMLDATA, xml);
		   editor.commit();
	   }
	   public String getStore(){
		   String xml = shareObject.getString(XMLDATA, null);
		   return xml;
	   }
}
