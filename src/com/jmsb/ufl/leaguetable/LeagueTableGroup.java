package com.jmsb.ufl.leaguetable;

import android.content.Intent;
import android.os.Bundle;
import com.jmsb.ufl.library.TabGroupActivity;

/**
 * Created with IntelliJ IDEA.
 * User: jmsb
 * Date: 8/2/13
 * Time: 12:22 PM
 * To change this template use File | Settings | File Templates.
 */
public class LeagueTableGroup extends TabGroupActivity {

    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        startChildActivity("LeagueTableActivity", new Intent(this, LeagueTable.class));
    }

}

