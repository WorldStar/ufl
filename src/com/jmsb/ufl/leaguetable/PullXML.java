package com.jmsb.ufl.leaguetable;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;


public class PullXML {
	public static List<HashMap<String,String>> main (String string)
	throws XmlPullParserException, IOException
	{
		List<HashMap<String, String>> resultMaps = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> temp = new HashMap<String, String>();
		string ="<?xml version='1.0' encoding='utf-8'?>" + string;
		XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
		factory.setNamespaceAware(true);
		XmlPullParser xpp = factory.newPullParser();

		xpp.setInput( new StringReader(string));
		int num = 0;
		try {
		    int eventType = xpp.getEventType();

		    while (eventType != XmlPullParser.END_DOCUMENT) {
		        String name = null;

		        switch (eventType){
		            case XmlPullParser.START_TAG:
		                name = xpp.getName().toLowerCase();

		                if (name.equals("group")) {
							temp = new HashMap<String, String>();
		                    for (int i = 0;i < xpp.getAttributeCount();i++) {
		                        String attribute = xpp.getAttributeName(i).toLowerCase();

		                    	if (attribute.equals("name")) {
		                    	    String value = xpp.getAttributeValue(i);
		                    	    temp.put(attribute, value);
		                        }

		                    }
		                    temp.put("group", "");
		                    resultMaps.add(temp);
		                    num = 0;
		                }else if(name.equals("team")){
		                	num++;
		                	temp = new HashMap<String, String>();
		                    for (int i = 0;i < xpp.getAttributeCount();i++) {
		                        String attribute = xpp.getAttributeName(i).toLowerCase();

		                    	//if (attribute.equals("name")) {
		                    	    String value = xpp.getAttributeValue(i);
		                    	    temp.put(attribute, value);
		                       // }else if()

		                    }
		                    temp.put("team", "");
		                    temp.put("num", ""+num);
		                    resultMaps.add(temp);
		                }

		                break;
		            case XmlPullParser.END_TAG:
		                name = xpp.getName();
		                break;
		        }

		        eventType = xpp.next();
		    }
		}
		catch (XmlPullParserException e) {
		    throw new RuntimeException("Cannot parse XML");
		}
		catch (IOException e) {
		    throw new RuntimeException("Cannot parse XML");
		}
		
		//add last element if not empty		
		String a = resultMaps.toString();
		return resultMaps;
	}
	public static List<HashMap<String,String>> main1 (String string)
			throws XmlPullParserException, IOException
	{
		XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
		factory.setNamespaceAware(true);
		XmlPullParser xpp = factory.newPullParser();

		xpp.setInput( new StringReader(string));
		int eventType = xpp.getEventType();
		String tag = "";
		String value = "";
		List<HashMap<String, String>> resultMaps = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> temp = new HashMap<String, String>();
		
		boolean save = false;
		int i = 0;
		while (eventType != XmlPullParser.END_DOCUMENT) {
			
			if(eventType == XmlPullParser.START_TAG) {
				
				tag = xpp.getName();
				
				if(tag.compareTo("tournament") == 0)
					save = true;
				
			} else if(eventType == XmlPullParser.TEXT) {
				
				value = xpp.getText();
				
				if (value == null)
					value = "";
				
				//only save the value under DocumentElement
				if (save){
					
					//if already have the same key, put it into array
					if (temp.containsKey(tag)){
						
						resultMaps.add(temp);
						temp = new HashMap<String, String>();
						
					}
					temp.put(tag, value);
				}
				
			} else if(eventType == XmlPullParser.END_TAG) {

				if(xpp.getName().compareTo("tournament") == 0){
					save = false;
					i++;
				}
			} 
			
			eventType = xpp.next();
		}
		
		//add last element if not empty
		if (!temp.isEmpty()){
			resultMaps.add(temp);
			
		}
		
		return resultMaps;
	}
}
