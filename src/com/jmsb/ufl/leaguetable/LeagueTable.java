package com.jmsb.ufl.leaguetable;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.*;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TableRow;
import android.widget.TextView;

import com.jmsb.ufl.home.Home.LoadHome;
import com.jmsb.ufl.leaguetable.PullXML;
import com.jmsb.ufl.leaguetable.SharedData;
//import com.google.android.maps.MapView.LayoutParams;
import com.jmsb.ufl.R;
import com.jmsb.ufl.library.DBHelper;
import com.jmsb.ufl.library.ReturnMessage;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlPullParserException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: jmsb
 * Date: 8/2/13
 * Time: 12:22 PM
 * To change this template use File | Settings | File Templates.
 */
public class LeagueTable extends Activity {
    DBHelper db;
    GroupTableObject gto;
    List<HashMap<String, String>> resultlist = new ArrayList<HashMap<String, String>>();
    boolean connctionflg= false;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.leaguetable);
        Typeface tf = Typeface.createFromAsset(getAssets(), "font/Helvetica.otf");
        TextView lheader = (TextView)findViewById(R.id.leagueheader); 
       // lheader.setTypeface(tf, Typeface.BOLD);
        db = new DBHelper(getContext());
        new LoadLeagueTable(getContext()).execute();
    }

    private Context getContext() {
        Context context;
        if (getParent() != null) context = getParent();
        else context = this;
        return context;
    }

    private class LoadLeagueTable extends AsyncTask<Void, Void, Void> {
        ProgressDialog progressDialog;
        Context context;

        private LoadLeagueTable(Context context){
            super();
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setTitle(getString(R.string.LoadingDialog_Title));
            progressDialog.setMessage(getString(R.string.LoadingDialog_Content1));
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
        	
        	resultlist = getContentFromXML(getString(R.string.LeagueTableUrl), "group");
            //List<GroupTableObject> groupList = null;
        	//NodeList teamNodeList = getContentFromXML(getString(R.string.LeagueTableUrl), "team");
            //if(groupNodeList != null){
            	//db.dropGroupTable();
            	//groupList = convertGroupNodeToObjectList(groupNodeList);
            //}
           // if(teamNodeList != null){
                //db.dropLeagueTable();
                //List<LeagueTableObject> teamList = convertNodeToObjectList(teamNodeList);
            //}
            return null;  //To change body of implemented methods use File | Settings | File Templates.
        }

		@Override
        protected void onPostExecute(Void v) {
            super.onPostExecute(v);
            progressDialog.dismiss();
            //List<LeagueTableObject> list = db.getLeagueTable();
            if(resultlist != null){
                showLeagueTable(context, resultlist);
            }else{
                //ReturnMessage.showAlertDialog(context, "Unable to load", "Unable to load League Table, please try again later.");
            	AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("Unable to load").setCancelable(false);
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                    	dialog.dismiss();
                    }
                });
                builder.setPositiveButton(getString(R.string.InternetNotFound_DialogPositiveButton), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        new LoadLeagueTable(context).execute();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        }

        private void showLeagueTable(Context context, List<HashMap<String, String>> list) {
            ListView leaguetableListView = (ListView)findViewById(R.id.listview);
            //LeagueTableAdapter leagueTableAdapter = new LeagueTableAdapter(context, 0, list);
            //leaguetableListView.setAdapter(leagueTableAdapter);
            String[] from = new String[] { "name" };
    		int[] to = new int[] { R.id.leaguetable_position };
    		LeagueTableAdapter leagueTableAdapter = new LeagueTableAdapter(context, resultlist,
    				R.layout.leaguetable_row, from, to);
    		leaguetableListView.setAdapter(leagueTableAdapter);
        }
    }

    private class LeagueTableAdapter extends SimpleAdapter{
        Context context;
        private int num = 0;
        private List<HashMap<String, String>> results;
        public class LeagueTableHolder{
            TextView position;
            TextView team;
            TextView played;
            TextView won;
            TextView draw;
            TextView lost;
            TextView goal;
            TextView diff;
            TextView pts;
            ImageView teamlogo;
        }

        public LeagueTableAdapter(Context context, List<HashMap<String,String>> _result, int resource, String[] from, int[] to) {
			super(context, _result, resource, from, to);
            this.context = context;
			this.results = _result;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LeagueTableHolder holder;
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            Typeface tf = Typeface.createFromAsset(getAssets(), "font/Helvetica.otf");
            if(convertView == null){
                convertView = inflater.inflate(R.layout.leaguetable_row, parent, false);
                holder = new LeagueTableHolder();
                holder.position = (TextView)convertView.findViewById(R.id.leaguetable_position);
                //holder.position.setTypeface(tf);
                holder.team = (TextView)convertView.findViewById(R.id.leaguetable_name);
                //holder.team.setTypeface(tf);
                holder.played = (TextView)convertView.findViewById(R.id.leaguetable_played);
                //holder.played.setTypeface(tf);
                holder.won = (TextView)convertView.findViewById(R.id.leaguetable_won);
                //holder.won.setTypeface(tf);
                holder.draw = (TextView)convertView.findViewById(R.id.leaguetable_draw);
                //holder.draw.setTypeface(tf);
                holder.lost = (TextView)convertView.findViewById(R.id.leaguetable_lost);
                //holder.lost.setTypeface(tf);
                holder.goal = (TextView)convertView.findViewById(R.id.leaguetable_goal);
                //holder.goal.setTypeface(tf);
                holder.diff = (TextView)convertView.findViewById(R.id.leaguetable_diff);
                //holder.diff.setTypeface(tf);
                holder.pts = (TextView)convertView.findViewById(R.id.leaguetable_pts);
                //holder.pts.setTypeface(tf);
                holder.teamlogo = (ImageView)convertView.findViewById(R.id.ivLocalImage);
                convertView.setTag(holder);
            }
            else
                holder = (LeagueTableHolder)convertView.getTag();
            HashMap<String, String> leagueTable = results.get(position);
            if(leagueTable.get("group") != null){
                TableRow tablerow = (TableRow)convertView.findViewById(R.id.tabelrow);
                tablerow.setVisibility(View.GONE);
                LinearLayout groupname = (LinearLayout)convertView.findViewById(R.id.grouptitle);
                groupname.setVisibility(View.VISIBLE);
                TextView text = (TextView)convertView.findViewById(R.id.textView1);
                text.setText("Group " + leagueTable.get("name"));
                text.setTextColor(Color.WHITE);
                //text.setTypeface(tf);
            }else{
                TableRow tablerow = (TableRow)convertView.findViewById(R.id.tabelrow);
                tablerow.setVisibility(View.VISIBLE);
                LinearLayout groupname = (LinearLayout)convertView.findViewById(R.id.grouptitle);
                groupname.setVisibility(View.GONE);
                
                
                holder.position.setText(leagueTable.get("num"));
                //holder.position.setTypeface(tf);
                holder.team.setText(leagueTable.get("name"));
                //holder.team.setTypeface(tf);
                holder.played.setText(leagueTable.get("gp"));
                //holder.played.setTypeface(tf);
                holder.won.setText(leagueTable.get("w"));
                //holder.won.setTypeface(tf);
                holder.draw.setText(leagueTable.get("d"));
                //holder.draw.setTypeface(tf);
                holder.lost.setText(leagueTable.get("l"));
                //holder.lost.setTypeface(tf);
                holder.goal.setText(leagueTable.get("gs")+":"+leagueTable.get("ga"));
                //holder.goal.setTypeface(tf);
                String diff = "";
                try{
                	int a = Integer.parseInt(leagueTable.get("gs"))-Integer.parseInt(leagueTable.get("ga"));
                	diff = "" + a;
                }catch(Exception e){
                	
                }
                holder.diff.setText(diff);
                holder.pts.setText(leagueTable.get("p"));
                File imgFile = new File(Environment.getExternalStorageDirectory().toString()+"/TaiwanIntercity/"+leagueTable.get("id")+".png");
                String filename = Environment.getExternalStorageDirectory().toString()+"/TaiwanIntercity/"+leagueTable.get("id")+".png";
                

                
                //File imgFile = new File(Environment.getDataDirectory().toString()+"/TaiwanIntercity/"+leagueTableObject.getTeamID()+".png");
                if(imgFile.exists()){
                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    holder.teamlogo.setImageBitmap(myBitmap);
                }else{
                	imgFile=new File(getApplicationContext().getFilesDir().getAbsolutePath()+"/TaiwanIntercity/"+leagueTable.get("id")+".png");
                	if(imgFile.exists()){
                        Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                        holder.teamlogo.setImageBitmap(myBitmap);
                    }else{
                    	holder.teamlogo.setImageResource(R.drawable.rihanna);
                    }
                }
            }
            return convertView;
        }
    }

    /*private List<GroupTableObject> convertGroupNodeToObjectList(NodeList nodeList) {
		// TODO Auto-generated method stub
    	Node node = null;
    	List<GroupTableObject> groupList= new ArrayList<GroupTableObject>();
    	GroupTableObject group;
    	int nodeListSize = nodeList.getLength();
    	for(int i=0; i<nodeListSize; i++){
    		node = nodeList.item(i);
    		group = new GroupTableObject();
    		group.setGroup_name(((Element) node).getAttribute("groupname"));
    		if(group.group_name=="A"){
    			group.setSize(5);
    		}
    		else if(group.group_name=="B"){
    			group.setSize(4);
    		}
    		else if(group.group_name=="C"){
    			group.setSize(5);
    		}
    		else if(group.group_name=="D"){
    			group.setSize(4);
    		}
    		else{
    			group.setSize(5);
    		}
    		groupList.add(group);
    	}
		return groupList;
	}*/

	private List<LeagueTableObject> convertNodeToObjectList(NodeList nodeList) {
        Node node = null;
        List<LeagueTableObject> teamList= new ArrayList<LeagueTableObject>();
        LeagueTableObject team;
        int nodeListSize = nodeList.getLength();
        for(int i=0; i<nodeListSize; i++){
            node = nodeList.item(i);
            team = new LeagueTableObject();
            team.setPosition(i+1);
            team.setTeam_name(((Element) node).getAttribute("name"));
            team.setGame_played(Integer.parseInt(((Element) node).getAttribute("gp")));
            team.setGame_won(Integer.parseInt(((Element) node).getAttribute("w")));
            team.setGame_draw(Integer.parseInt(((Element) node).getAttribute("d")));
            team.setGame_lost(Integer.parseInt(((Element)node).getAttribute("l")));
            team.setGoal_scored(Integer.parseInt(((Element)node).getAttribute("gs")));
            team.setGoal_against(Integer.parseInt(((Element)node).getAttribute("ga")));
            team.setPoint(Integer.parseInt(((Element)node).getAttribute("p")));
            team.setTeamID(((Element)node).getAttribute("id"));
            teamList.add(i,team);
            db.setLeagueTable(team);
        	}
        return teamList;
    }

    private List<HashMap<String, String>> getContentFromXML(String urlString, String tagName) {
    	List<HashMap<String, String>> resultlist = new ArrayList<HashMap<String, String>>();
    	ConnectivityManager conMgr = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo i = conMgr.getActiveNetworkInfo();
    	if (i != null && i.getState()==NetworkInfo.State.CONNECTED) {
        try {
            URL url = new URL(urlString);
            URLConnection con = url.openConnection();
            con.setReadTimeout(30000);
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(con.getInputStream());
            doc.getDocumentElement().normalize();
            
            //nodeList = doc.getElementsByTagName(tagName);
            

            //set up a transformer
            TransformerFactory transfac = TransformerFactory.newInstance();
            Transformer trans = transfac.newTransformer();
            trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            trans.setOutputProperty(OutputKeys.INDENT, "yes");

            //create string from xml tree
            StringWriter sw = new StringWriter();
            StreamResult result = new StreamResult(sw);
            DOMSource source = new DOMSource(doc);
            trans.transform(source, result);
            String xmlString = sw.toString();
            /************* store xml data in*/ 
            SharedData store = new SharedData(this);
            store.setStore(xmlString);
            /***************/
            resultlist = PullXML.main(xmlString);
        }catch (SocketTimeoutException ex){
        	resultlist = null;
        	connctionflg = false;
        }catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch(TransformerConfigurationException e){
        	e.printStackTrace();
        } catch(TransformerException e){
        	e.printStackTrace();
        } catch(XmlPullParserException e){
        	e.printStackTrace();
        }
    	}else{
        	try{
	            SharedData store = new SharedData(this);
	            String xmlString  = store.getStore();
	            resultlist = PullXML.main(xmlString);
	        } catch (MalformedURLException e) {
	            e.printStackTrace();
	            e.printStackTrace();
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        	e.printStackTrace();
	        	e.printStackTrace();
	        } catch(XmlPullParserException e){
	        	e.printStackTrace();
	        }
    	}
		return resultlist;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.option_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_refresh:
                new LoadLeagueTable(getContext()).execute();
                break;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage(getString(R.string.ExitDialog_Content)).setCancelable(false);
        builder.setNegativeButton(getString(R.string.ExitDialog_NoButton), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.setPositiveButton(getString(R.string.ExitDialog_YesButton), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }
}