package com.jmsb.ufl.schedule;

/**
 * Created with IntelliJ IDEA.
 * User: jmsb
 * Date: 8/5/13
 * Time: 11:09 AM
 * To change this template use File | Settings | File Templates.
 */
public class ScheduleObject {
    String homeTeam;
    String awayTeam;
    String location;

    public int getMatchNumber() {
        return matchNumber;
    }

    public void setMatchNumber(int matchNumber) {
        this.matchNumber = matchNumber;
    }

    int matchNumber;

    public void setHomeTeam(String homeTeam) {
        this.homeTeam = homeTeam;
    }

    public void setAwayTeam(String awayTeam) {
        this.awayTeam = awayTeam;
    }

    public void setHomeScore(String homeScore) {
        this.homeScore = homeScore;
    }

    public void setAwayScore(String awayScore) {
        this.awayScore = awayScore;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setHomeTeamID(String homeTeamID) {
        this.homeTeamID = homeTeamID;
    }

    public void setAwayTeamID(String awayTeamID) {
        this.awayTeamID = awayTeamID;
    }

    String homeScore;
    String awayScore;
    String date;
    String time;
    String homeTeamID;
    String awayTeamID;
    int matchDay;

    public int getMatchDay() {
        return matchDay;
    }

    public void setMatchDay(int matchDay) {
        this.matchDay = matchDay;
    }

    public String getAwayTeamID() {
        return awayTeamID;
    }

    public String getHomeTeamID() {
        return homeTeamID;
    }

    public void setHomeAndAwayTeamID(String homeAndAwayTeamID) {
        String homeAwayTeamID[] = homeAndAwayTeamID.split("-");
        this.homeTeamID = homeAwayTeamID[0];
        this.awayTeamID = homeAwayTeamID[1];
    }

    public String getTime() {
        return time;
    }

    public String getDate() {
        return date;
    }
    
    public void setdate(String date){
    	this.date = date;
    }
    public void setDateAndTime(String dateAndTime) {
        int index = 0;
        try{
         String mon = "", day = "", yy= "";
         if((index = dateAndTime.indexOf(" "))!=-1){
          String a = dateAndTime.substring(0, index);
          dateAndTime = dateAndTime.substring(index+1, dateAndTime.length());
          if(a.indexOf("Jan") !=-1){
           mon = "01";
          }else if(a.indexOf("Jan") !=-1){
           mon = "01";
          }else if(a.indexOf("Feb") !=-1){
           mon = "02";
          }else if(a.indexOf("Mar") !=-1){
           mon = "03";
          }else if(a.indexOf("Apr") !=-1){
           mon = "04";
          }else if(a.indexOf("May") !=-1){
           mon = "05";
          }else if(a.indexOf("Jun") !=-1){
           mon = "06";
          }else if(a.indexOf("Jul") !=-1){
           mon = "07";
          }else if(a.indexOf("Aug") !=-1){
           mon = "08";
          }else if(a.indexOf("Sept") !=-1){
           mon = "09";
          }else if(a.indexOf("Oct") !=-1){
           mon = "10";
          }else if(a.indexOf("Nov") !=-1){
           mon = "11";
          }else if(a.indexOf("Dec") !=-1){
           mon = "12";
          }
         }
         if((index = dateAndTime.indexOf(", "))!=-1){
          day = dateAndTime.substring(0,index);
          yy = dateAndTime.substring(index+2, dateAndTime.length());
         }
          this.date = yy+"-"+mon+"-"+day;
        }catch(Exception e){
         this.date = "00-00-00";
        }
   }
   public void setDateAndTime1(String dateAndTime) {
       this.time = dateAndTime;	
   }
//    public void setDateAndTime(String dateAndTime) {
//        String dateTime[] = dateAndTime.split(" ");
//        this.date = dateTime[0];
//        this.time = dateTime[1];
//    }

    public String getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeamAndAwayTeam(String homeTeamAndAwayTeam) {
        String homeAwayTeam[] = homeTeamAndAwayTeam.split("-");
        this.homeTeam = homeAwayTeam[0];
        this.awayTeam = homeAwayTeam[1];
    }

    public String getAwayTeam() {
        return awayTeam;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getHomeScore() {
        return homeScore;
    }

    public void setHomeScoreAndAwayScore(String homeScoreAndAwayScore) {
        String homeAwayScore[] = homeScoreAndAwayScore.split("-");
        if(homeAwayScore.length!=0){
            this.homeScore = homeAwayScore[0];
            this.awayScore = homeAwayScore[1];
        }else{
            this.homeScore = " ";
            this.awayScore = " ";
        }
    }

    public String getAwayScore() {
        return awayScore;
    }
}
