package com.jmsb.ufl.schedule;

import android.content.Intent;
import android.os.Bundle;

import com.jmsb.ufl.library.TabGroupActivity;

public class ScheduleTabGroup extends TabGroupActivity{
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		startChildActivity("ScheduleActivity", new Intent(this, Schedule.class));
	}
}