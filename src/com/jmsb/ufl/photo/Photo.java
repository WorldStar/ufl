package com.jmsb.ufl.photo;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import com.jmsb.ufl.R;

/**
 * Created with IntelliJ IDEA.
 * User: jmsb
 * Date: 8/16/13
 * Time: 4:09 PM
 * To change this template use File | Settings | File Templates.
 */
public class Photo extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photo);
        Typeface tf = Typeface.createFromAsset(getAssets(), "font/Helvetica.otf");
        TextView header = (TextView)findViewById(R.id.header);
        //header.setTypeface(tf, Typeface.BOLD);
        header.setText(getString(R.string.HeaderTitle_Photo));
        WebView webView = (WebView)findViewById(R.id.webview);
        webView.setWebViewClient(new WebViewClient(){
            ProgressDialog progressDialog;
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon)
            {
                progressDialog = new ProgressDialog(Photo.this);
                progressDialog.setTitle(getString(R.string.LoadingDialog_Title));
                progressDialog.setMessage(getString(R.string.LoadingDialog_Content1));
                progressDialog.show();
            }
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url)
            {
                view.loadUrl(url);
                return true;
            }
            @Override
            public void onPageFinished(WebView view, String url)
            {
                progressDialog.dismiss();
            }
        });
        webView.loadUrl("http://www.interaksyon.com/interaktv/photos/football/");
    }
    private class Callback extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return (false);
        }

    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Photo.this);
        builder.setMessage(getString(R.string.ExitDialog_Content)).setCancelable(false);
        builder.setNegativeButton(getString(R.string.ExitDialog_NoButton), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.setPositiveButton(getString(R.string.ExitDialog_YesButton), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }
}