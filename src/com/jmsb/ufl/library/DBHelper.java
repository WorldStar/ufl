package com.jmsb.ufl.library;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.jmsb.ufl.leaguetable.GroupTableObject;
import com.jmsb.ufl.leaguetable.LeagueTableObject;
import com.jmsb.ufl.news.NewsObject;
import com.jmsb.ufl.schedule.ScheduleObject;


import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: jmsb
 * Date: 8/12/13
 * Time: 2:31 PM
 * To change this template use File | Settings | File Templates.
 */
public class DBHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "UFLDB";
    private static final String TABLE_SCHEDULE = "schedule_table";
    private static final String TABLE_NEWS = "news_table";
    private static final String TABLE_LEAGUETABLE = "league_table";
    //private static final String TABLE_VIDEO = "video_table";

    // Table Columns names for schedule
    private static final String KEY_SCHEDULE_ID = "rowid";
    private static final String KEY_SCHEDULE_DATE = "date";
    private static final String KEY_SCHEDULE_TIME = "time";
    private static final String KEY_SCHEDULE_HOMETEAM = "home_team";
    private static final String KEY_SCHEDULE_AWAYTEAM = "away_team";
    private static final String KEY_SCHEDULE_HOMESCORE = "home_score";
    private static final String KEY_SCHEDULE_AWAYSCORE = "away_score";
    private static final String KEY_SCHEDULE_HOMEID = "home_id";
    private static final String KEY_SCHEDULE_AWAYID = "away_id";
    private static final String KEY_SCHEDULE_MATCHDAY = "match_day";
    private static final String KEY_SCHEDULE_MATCHNUMBER = "match_number";

    // Table Columns names for news
    private static final String KEY_NEWS_TITLE = "title";
    private static final String KEY_NEWS_DATE = "date";
    private static final String KEY_NEWS_CONTENT = "content";
    private static final String KEY_NEWS_IMAGEURL = "imageUrl";
    private static final String KEY_NEWS_LISTCONTENT = "list_content";

    // Table Columns names for leaguetable
    private static final String KEY_TABLE_POSITION = "position";
    private static final String KEY_TABLE_NAME = "name";
    private static final String KEY_TABLE_PLAYED = "game_played";
    private static final String KEY_TABLE_WON = "game_won";
    private static final String KEY_TABLE_DRAW = "game_draw";
    private static final String KEY_TABLE_LOST = "game_lost";
    private static final String KEY_TABLE_SCORED = "goal_scored";
    private static final String KEY_TABLE_AGAINST = "goal_against";
    private static final String KEY_TABLE_POINTS = "points";
    private static final String KEY_TABLE_ID = "team_id";

    // Table Columns names for video
    /*private static final String KEY_VIDEO_TITLE = "title";
    private static final String KEY_VIDEO_CONTENT = "content";
    private static final String KEY_VIDEO_VIDEOURL = "videoUrl";
    private static final String KEY_VIDEO_IMAGEURL = "imageUrl";*/

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_SCHEDULE_TABLE = "CREATE TABLE " + TABLE_SCHEDULE + "("
                + KEY_SCHEDULE_ID + " INTEGER PRIMARY KEY,"
                + KEY_SCHEDULE_DATE + " TEXT," + KEY_SCHEDULE_TIME + " TEXT,"
                + KEY_SCHEDULE_HOMETEAM + " TEXT," + KEY_SCHEDULE_AWAYTEAM + " TEXT,"
                + KEY_SCHEDULE_HOMESCORE + " TEXT," + KEY_SCHEDULE_AWAYSCORE + " TEXT,"
                + KEY_SCHEDULE_HOMEID + " TEXT," + KEY_SCHEDULE_AWAYID + " TEXT,"
                + KEY_SCHEDULE_MATCHDAY + " TEXT,"+ KEY_SCHEDULE_MATCHNUMBER + " TEXT" + ")";
        String CREATE_LEAGUETABLE_TABLE = "CREATE TABLE " + TABLE_LEAGUETABLE + "("
                + KEY_TABLE_POSITION + " TEXT," + KEY_TABLE_NAME + " TEXT,"
                + KEY_TABLE_PLAYED + " TEXT," + KEY_TABLE_WON + " TEXT,"
                + KEY_TABLE_DRAW + " TEXT," + KEY_TABLE_LOST + " TEXT,"
                + KEY_TABLE_SCORED + " TEXT," + KEY_TABLE_AGAINST + " TEXT,"
                + KEY_TABLE_POINTS + " TEXT," + KEY_TABLE_ID + " TEXT" + ")";
        String CREATE_NEWS_TABLE = "CREATE TABLE " + TABLE_NEWS + "("
                + KEY_NEWS_TITLE + " TEXT," + KEY_NEWS_DATE + " TEXT,"
                + KEY_NEWS_CONTENT + " TEXT," + KEY_NEWS_IMAGEURL + " TEXT,"
                + KEY_NEWS_LISTCONTENT + " TEXT" + ")";
       /*String CREATE_VIDEO_TABLE = "CREATE TABLE " + TABLE_VIDEO + "("
                + KEY_VIDEO_TITLE + " TEXT," + KEY_VIDEO_CONTENT + " TEXT,"
                + KEY_VIDEO_VIDEOURL + " TEXT," + KEY_VIDEO_IMAGEURL + " TEXT)";*/
        try{
        sqLiteDatabase.execSQL(CREATE_SCHEDULE_TABLE);
        }catch (Exception e){
            //Table exist
        }
        try{
        sqLiteDatabase.execSQL(CREATE_NEWS_TABLE);
        }catch (Exception e){
            //Table exist
        }
        try{
            sqLiteDatabase.execSQL(CREATE_LEAGUETABLE_TABLE);
        }catch (Exception e){
            //Table exist
        }
        /*try{
            sqLiteDatabase.execSQL(CREATE_VIDEO_TABLE);
        }catch (Exception e){
            //Table exist
        }*/

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {
        // Drop older table if existed
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_SCHEDULE);
        // Create tables again
        onCreate(sqLiteDatabase);
    }

    public void dropScheduleTable(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SCHEDULE);
        onCreate(db);
    }

    public void dropNewsTable(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NEWS);
        onCreate(db);
    }

    public void dropLeagueTable(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LEAGUETABLE);
        onCreate(db);
    }
    
    /*public void dropVideoTable(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_VIDEO);
        onCreate(db);
    }*/

    public void setSchedule(ScheduleObject scheduleObject){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_SCHEDULE_DATE, scheduleObject.getDate());//0
        values.put(KEY_SCHEDULE_TIME, scheduleObject.getTime());//1
        values.put(KEY_SCHEDULE_HOMETEAM, scheduleObject.getHomeTeam());//2
        values.put(KEY_SCHEDULE_AWAYTEAM, scheduleObject.getAwayTeam());//3
        values.put(KEY_SCHEDULE_HOMESCORE, scheduleObject.getHomeScore());//4
        values.put(KEY_SCHEDULE_AWAYSCORE, scheduleObject.getAwayScore());//5
        values.put(KEY_SCHEDULE_HOMEID, scheduleObject.getHomeTeamID());//6
        values.put(KEY_SCHEDULE_AWAYID, scheduleObject.getAwayTeamID());//7
        values.put(KEY_SCHEDULE_MATCHDAY, scheduleObject.getMatchDay());//8
        values.put(KEY_SCHEDULE_MATCHNUMBER, scheduleObject.getMatchNumber()); //9
        // Inserting Row
        db.insert(TABLE_SCHEDULE, null, values);
        db.close(); // Closing database connection
    }

    public void setNews(NewsObject newsObject){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_NEWS_TITLE, newsObject.getTitle());//0
        values.put(KEY_NEWS_DATE, newsObject.getDate());//1
        values.put(KEY_NEWS_CONTENT, newsObject.getContent());//2
        values.put(KEY_NEWS_IMAGEURL, newsObject.getImageUrl());//3
        values.put(KEY_NEWS_LISTCONTENT, newsObject.getListContent());
        // Inserting Row
        db.insert(TABLE_NEWS, null, values);
        db.close(); // Closing database connection
    }

    public void setLeagueTable(LeagueTableObject leagueTableObject){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_TABLE_POSITION, leagueTableObject.getPosition());//0
        values.put(KEY_TABLE_NAME, leagueTableObject.getTeam_name());//1
        values.put(KEY_TABLE_PLAYED, leagueTableObject.getGame_played());//2
        values.put(KEY_TABLE_WON, leagueTableObject.getGame_won());//3
        values.put(KEY_TABLE_DRAW, leagueTableObject.getGame_draw());
        values.put(KEY_TABLE_LOST, leagueTableObject.getGame_lost());
        values.put(KEY_TABLE_SCORED, leagueTableObject.getGoal_scored());
        values.put(KEY_TABLE_AGAINST, leagueTableObject.getGoal_against());
        values.put(KEY_TABLE_POINTS, leagueTableObject.getPoint());
        values.put(KEY_TABLE_ID, leagueTableObject.getTeamID());
        // Inserting Row
        db.insert(TABLE_LEAGUETABLE, null, values);
        db.close(); // Closing database connection
    }

    /*public void setVideo(VideoObject videoObject){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_VIDEO_TITLE, videoObject.getTitle());//0
        values.put(KEY_VIDEO_CONTENT, videoObject.getContent());//1
        values.put(KEY_VIDEO_VIDEOURL, videoObject.getVideoUrl());//2
        values.put(KEY_VIDEO_IMAGEURL, videoObject.getThumbnailUrl());//3
        // Inserting Row
        db.insert(TABLE_VIDEO, null, values);
        db.close(); // Closing database connection
    }*/

    public List<ScheduleObject> getSchedule(){
        List<ScheduleObject> scheduleObjectList = new ArrayList<ScheduleObject>();
        String selectQuery = "SELECT * FROM " + TABLE_SCHEDULE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery,null);
        if(cursor.moveToFirst()){
            do {
                ScheduleObject scheduleObject = new ScheduleObject();
                scheduleObject.setDate(cursor.getString(1));
                scheduleObject.setTime(cursor.getString(2));
                scheduleObject.setHomeTeam(cursor.getString(3));
                scheduleObject.setAwayTeam(cursor.getString(4));
                scheduleObject.setHomeScore(cursor.getString(5));
                scheduleObject.setAwayScore(cursor.getString(6));
                scheduleObject.setHomeTeamID(cursor.getString(7));
                scheduleObject.setAwayTeamID(cursor.getString(8));
                scheduleObject.setMatchDay(cursor.getInt(9));
                scheduleObject.setMatchNumber(cursor.getInt(10));
                scheduleObjectList.add(scheduleObject);
            }while(cursor.moveToNext());
        }
        return scheduleObjectList;
    }

    public List<ScheduleObject> getSchedule(boolean isDescending){
        List<ScheduleObject> scheduleObjectList = new ArrayList<ScheduleObject>();
        String selectQuery;
        if(isDescending)
            selectQuery = "SELECT * FROM " + TABLE_SCHEDULE + " ORDER BY ROWID DESC";
        else
            selectQuery = "SELECT * FROM " + TABLE_SCHEDULE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery,null);
        if(cursor.moveToFirst()){
            do {
                ScheduleObject scheduleObject = new ScheduleObject();
                scheduleObject.setDate(cursor.getString(1));
                scheduleObject.setTime(cursor.getString(2));
                scheduleObject.setHomeTeam(cursor.getString(3));
                scheduleObject.setAwayTeam(cursor.getString(4));
                scheduleObject.setHomeScore(cursor.getString(5));
                scheduleObject.setAwayScore(cursor.getString(6));
                scheduleObject.setHomeTeamID(cursor.getString(7));
                scheduleObject.setAwayTeamID(cursor.getString(8));
                scheduleObject.setMatchDay(cursor.getInt(9));
                scheduleObject.setMatchNumber(cursor.getInt(10));
                scheduleObjectList.add(scheduleObject);
            }while(cursor.moveToNext());
        }
        return scheduleObjectList;
    }

    public List<NewsObject> getNews(){
        List<NewsObject> newsObjectList = new ArrayList<NewsObject>();
        String selectQuery = "SELECT * FROM " + TABLE_NEWS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery,null);
        if(cursor.moveToFirst()){
            do {
                NewsObject newsObject = new NewsObject();
                newsObject.setTitle(cursor.getString(0));
                newsObject.setDate(cursor.getString(1));
                newsObject.setContent(cursor.getString(2));
                newsObject.setImageUrl(cursor.getString(3));
                newsObject.setListContent(cursor.getString(4));
                newsObjectList.add(newsObject);
            }while(cursor.moveToNext());
        }
        return newsObjectList;
    }

    public List<LeagueTableObject> getLeagueTable(){
        List<LeagueTableObject> leagueTableObjectList = new ArrayList<LeagueTableObject>();
        String selectQuery = "SELECT * FROM " + TABLE_LEAGUETABLE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery,null);
        if(cursor.moveToFirst()){
            do {
                LeagueTableObject leagueTableObject = new LeagueTableObject();
                leagueTableObject.setPosition(cursor.getInt(0));
                leagueTableObject.setTeam_name(cursor.getString(1));
                leagueTableObject.setGame_played(cursor.getInt(2));
                leagueTableObject.setGame_won(cursor.getInt(3));
                leagueTableObject.setGame_draw(cursor.getInt(4));
                leagueTableObject.setGame_lost(cursor.getInt(5));
                leagueTableObject.setGoal_scored(cursor.getInt(6));
                leagueTableObject.setGoal_against(cursor.getInt(7));
                leagueTableObject.setPoint(cursor.getInt(8));
                leagueTableObject.setTeamID(cursor.getString(9));
                leagueTableObjectList.add(leagueTableObject);
            }while(cursor.moveToNext());
        }
        return leagueTableObjectList;
    }

    /*public List<VideoObject> getVideo(){
        List<VideoObject> videoObjectList = new ArrayList<VideoObject>();
        String selectQuery = "SELECT * FROM " + TABLE_VIDEO;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery,null);
        if(cursor.moveToFirst()){
            do {
                VideoObject videoObject = new VideoObject();
                videoObject.setTitle(cursor.getString(0));
                videoObject.setContent(cursor.getString(1));
                videoObject.setVideoUrl(cursor.getString(2));
                videoObject.setThumbnailUrl(cursor.getString(3));
                videoObjectList.add(videoObject);
            }while(cursor.moveToNext());
        }
        return videoObjectList;
    }*/
}
