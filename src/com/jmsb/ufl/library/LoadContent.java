package com.jmsb.ufl.library;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import com.jmsb.ufl.R;
import com.jmsb.ufl.news.NewsObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: jmsb
 * Date: 8/19/13
 * Time: 5:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class LoadContent {

    public static void LoadNews(Context context){
        new LoadNewsFromUrl(context).execute();
    }

    public static class LoadNewsFromUrl extends AsyncTask<Void, Void, List<NewsObject>> {
        Context context;
        ProgressDialog progressDialog;

        public LoadNewsFromUrl(Context context){
            super();
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setTitle(context.getString(R.string.LoadingDialog_Title));
            progressDialog.setMessage(context.getString(R.string.LoadingDialog_Content1));
            progressDialog.show();
        }

        @Override
        protected List<NewsObject> doInBackground(Void... voids) {
            List<NewsObject> newsObjectList = null;

            NodeList newsNodeList = getContentFromXML(context.getString(R.string.NewsUrl), "post");
            if(newsNodeList != null){
                newsObjectList = new ArrayList<NewsObject>();
                newsObjectList = convertNewsNodeToObjectList(newsNodeList);
            }
            return newsObjectList;
        }

        @Override
        protected void onPostExecute(List<NewsObject> list) {
            super.onPostExecute(list);
            DBHelper db = new DBHelper(context);
            if(list != null){
                db.dropNewsTable();
                NewsObject newsObject;
                for(int i=0;i<list.size();i++){
                    newsObject = list.get(i);
                    db.setNews(newsObject);
                }
            }
            progressDialog.dismiss();
        }
    }



    private static NodeList getContentFromXML(String urlString, String tagName) {
        NodeList nodeList = null;
        try {
            URL url = new URL(urlString);
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(new InputSource(url.openStream()));
            doc.getDocumentElement().normalize();
            nodeList = doc.getElementsByTagName(tagName);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return nodeList;
    }

    private static List<NewsObject> convertNewsNodeToObjectList(NodeList nodeList) {
        Node node = null;
        List<NewsObject> newsList= new ArrayList<NewsObject>();
        NewsObject news;
        String temp;
        int newsListSize = nodeList.getLength();
        for(int i=0; i<newsListSize; i++){
            node = nodeList.item(i);
            news = new NewsObject();
            news.setTitle(((Element) node).getAttribute("title"));
            news.setDate(((Element) node).getAttribute("publish_date"));
            //news.setContent(((Element) node).getAttribute("content"));
            Element fstElmnt = (Element) node;
            NodeList webContent = fstElmnt.getElementsByTagName("content");
            Element webContentElement = (Element) webContent.item(0);
            webContent = webContentElement.getChildNodes();
            try{
                news.setContent(((Node) webContent.item(0)).getNodeValue());
                temp = news.getContent();
                news.setListContent(temp.replaceAll("\\<.*?>","").replace("\n","").replace("\r",""));
            }catch (Exception e){
                news.setContent("");
            }
            news.setImageUrl(((Element)node).getAttribute("image"));
            newsList.add(news);
        }
        return newsList;
    }
}
