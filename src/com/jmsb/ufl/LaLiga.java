package com.jmsb.ufl;

import android.app.TabActivity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TabHost;
import android.widget.TextView;
import com.jmsb.ufl.home.HomeTabGroup;
import com.jmsb.ufl.leaguetable.LeagueTableGroup;
import com.jmsb.ufl.library.CustomizedScrollView;
import com.jmsb.ufl.library.ScrollViewListener;
import com.jmsb.ufl.news.NewsTabGroup;
import com.jmsb.ufl.photo.Photo;
import com.jmsb.ufl.result.ResultsTabGroup;
import com.jmsb.ufl.schedule.ScheduleTabGroup;


public class LaLiga extends TabActivity implements ScrollViewListener{
	TabHost tabHost;	
	private CustomizedScrollView hscrollView = null;
	ImageView leftNavi, rightNavi;
	Animation fadein, fadeout;
	/** Called when the activity is first created. */
    @SuppressWarnings("deprecation")
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tablayout);
        
        setTabs();
        
		hscrollView = (CustomizedScrollView)findViewById(R.id.hscrollbar);
		hscrollView.setScrollViewListener(this);
		leftNavi = (ImageView)findViewById(R.id.leftNavi);
		rightNavi = (ImageView)findViewById(R.id.rightNavi);
		fadein = AnimationUtils.loadAnimation(this, R.anim.fadein);
		fadeout = AnimationUtils.loadAnimation(this, R.anim.fadeout);
    }
    
    private void setTabs()
	{
		addTab(getString(R.string.TabTitle_Home), R.drawable.home_unselected, HomeTabGroup.class);
		addTab(getString(R.string.TabTitle_News), R.drawable.news_unselected, NewsTabGroup.class);
        addTab(getString(R.string.TabTitle_Photo), R.drawable.photo_unselected, Photo.class);
		addTab(getString(R.string.TabTitle_Schedules), R.drawable.fixtures_unselected, ScheduleTabGroup.class);
		addTab(getString(R.string.TabTitle_Results), R.drawable.result_unselected, ResultsTabGroup.class);
		addTab(getString(R.string.TabTitle_LeagueTable), R.drawable.standings_unselected, LeagueTableGroup.class);
		//addTab(getString(R.string.TabTitle_Video), R.drawable.video_unselected, VideoTabGroup.class);
        //addTab("Other", R.drawable.standings_unselected, Other.class);
		//addTab(getString(R.string.TabTitle_MakeJersey), R.drawable.joinnow_unselected, Jersey.class);
		//addTab(getString(R.string.TabTitle_StatDe), R.drawable.tlighticon, StadeTabGroup.class);
	}
    
    private void addTab(String labelId, int drawableId, Class<?> c)
	{
		tabHost = getTabHost();
		Intent intent = new Intent(this, c);
		TabHost.TabSpec spec = tabHost.newTabSpec("tab" + labelId);	
		
		View tabIndicator = LayoutInflater.from(this).inflate(R.layout.tab_indicator, getTabWidget(), false);
		TextView title = (TextView) tabIndicator.findViewById(R.id.title);
		title.setText(labelId);
		ImageView icon = (ImageView) tabIndicator.findViewById(R.id.icon);
		icon.setImageResource(drawableId);
		icon.setScaleType(ScaleType.MATRIX);
		spec.setIndicator(tabIndicator);
		spec.setContent(intent);
		tabHost.addTab(spec);
		tabHost.getTabWidget().setBackgroundColor(Color.BLACK);
	}
    
	@Override
	public void onScrollChanged(CustomizedScrollView scrollView, int x, int y,
			int oldx, int oldy) {
		// TODO Auto-generated method stub
		int maxX = hscrollView.getChildAt(0).getMeasuredWidth()-getWindowManager().getDefaultDisplay().getWidth();

		if(x == 0)
		{
			leftNavi.startAnimation(fadeout);
			leftNavi.setVisibility(ImageView.INVISIBLE);
		}		
		else if(x == maxX)
		{
			rightNavi.startAnimation(fadeout);
			rightNavi.setVisibility(ImageView.INVISIBLE);
		}
		else if (x!= 0 || x != maxX)
		{
			
			leftNavi.setVisibility(ImageView.VISIBLE);
			rightNavi.setVisibility(ImageView.VISIBLE);
		}
		else
		{
			leftNavi.startAnimation(fadein);
			rightNavi.startAnimation(fadein);
		}
	}
}