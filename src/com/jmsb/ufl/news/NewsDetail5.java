package com.jmsb.ufl.news;

import com.jmsb.ufl.R;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;



public class NewsDetail5 extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.newsdetail);

		TextView tvHeadline = (TextView) findViewById(R.id.newsdetailheadline);
		//ImageView ivImage = (ImageView) findViewById(R.id.newsdetailimage);
		TextView tvDate = (TextView) findViewById(R.id.newsdetailpublishdate);
		WebView wvContent = (WebView) findViewById(R.id.newsdetailwebview);
		String bodyStr = "<p>Barcelona romped to a 5-0 win over a resilient Rayo Vallecano but the two Madrid sides doggedly followed the result with wins of their own the following day.</p>" +
				"<p>By Suhas Bhat</p>" +
				"<p>The Catalan side were less than impressive against Celtic in the UEFA Champions League but got everything right against Rayo, putting pressure on title rivals Real Madrid and Atletico Madrid to follow suit.</p>" +
				"<p>Nonetheless, Los Merengues replied with a 5-0 win of their own against Real Mallorca while some diligent wing-play by the Atletico players helped them register a 3-1 win victory over Osasuna.</p>" +
				"<p>Elsewhere, Valencia continued to stutter and dropped points once again against Real Betis in a 1-0 defeat. Malaga were similarly disappointing as they could not find a way past Espanyol at the Estadi RCDE.</p>" +
				"<p>ESPNSTAR.com profiles the best players from the ninth round of La Liga fixtures using a 4-5-1 formation.</p>" +
				"<p>GK: Adrian (Real Betis)</p>" +
				"<p>The Verdiblancos goalkeeper put in a solid performance to twice deny Roberto Soldado and he made a smart save from a Tino Costa free-kick. His awareness and quick reflexes helped ensure Salva Sevilla�s goal was enough to secure a victory and they are now just a point adrift of current champions Real Madrid.</p>" +
				"<p>RB: Juanfran (Atletico Madrid)</p>" +
				"<p>Atletico benefitted from some good crossing by the right-back and his pinpoint lob into the box was duly controlled by Radamel Falcao for Atletico�s third late on in the game. The pass was wonderfully done and sent in at just the right moment to help his Colombian team-mate stay onside and score with his heel to extend his tally to ten goals in the league.</p>" +
				"<p>CB: Miranda (Atletico Madrid)</p>" +
				"<p>Earlier, it was Miranda who helped score the first goal for Atletico as he rose highest to nod in Gabi�s free-kick. He was solid defensively while also helping initiate several attacks in the game and thus produced a complete performance.</p>" +
				"<p>CB: Hector Moreno (Espanyol)</p>" +
				"<p>Malaga, probably tired from their mid-week Champions League win over AC Milan, were held at bay by a determined Espanyol defence. The home side were led admirably by Hector Moreno in the heart of defence. A diving tackle by the Mexican to dispossess Seba Fernandez when he was through on goal was probably the highlight of the defensive display.</p>" +
				"<p>LB: Jordi Alba (Barcelona)</p>" +
				"<p>The left-back sent in a tidy cutback to help Lionel Messi score his second goal and provided timely assistance to the centre-back pairing of Adriano and Javier Mascherano (Barcelona�s eighth different pairing this season).</p>" +
				"<p>CM: Cesc Fabregas (Barcelona)</p>" +
				"<p>Fabregas sent in a perfect through-ball to David Villa to open the scoring for the league leaders in the 21st minute, and he also helped score Barca�s fourth towards the end of the game when he casually tapped in Pedro�s right-wing cross.</p>" +
				"<p>With seven assists in La Liga the league so far, Fabregas looks like he could usurp the excellent duo of Andres Iniesta and Xavi and become the main creative force for the Blaugrana this season.</p>" +
				"<p>CM: Antonio Juan Rodriguez (Getafe)</p>" +
				"<p>Getafe were the other side, apart from Betis, who did not let the stature of their opponents unnerve them as they defeated Athletic Bilbao in a comprehensive 2-1 win. Juan Rodriguez helped his side grab the momentum in the game with a header off a Pedro Leon cross and was a continual threat with his running off the ball. Striker Alvaro Vazquez�s acrobatic volley sealed the deal early on in the second half.</p>" +
				"<p>AMR: Antoine Griezmann (Real Sociedad)</p>" +
				"<p>The Frenchman was impressive as he twice put his side ahead in the game against Real Valladolid on Monday and his second was a beauty of a goal. Unfortunately, Valladolid were more than up for the challenge and managed to secure a point at the Estadio Jose Zorrilla.</p>" +
				"<p>AML: Cristiano Ronaldo (Real Madrid)</p>" +
				"<p>Ronaldo appears to have finally gotten back into the grove and took care of Mallorca in emphatic fashion. He scored a brace and assisted Gonzalo Higuain in the game to restore parity in the league.</p>" +
				"<p>With the player nominated for the Ballon d�Or alongside Lionel Messi, the voters for football�s most prestigious award will likely have a hard time picking them apart if the duo continue to impress on a weekly basis.</p>" +
				"<p>But in terms of trophies won over the past year, Ronaldo�s La Liga winners� medal would definitely warrant greater consideration than Messi�s Copa del Rey triumph although both failed to lead their side to Champions League glory.</p>" +
				"<p>ST: Obafemi Martins (Levante)</p>" +
				"<p>The former Newcastle striker is enjoying life in the Spanish first division and scored two goals to ensure Levante downed lowly Granada. The Nigerian bullied the defence and punished them for dallying on the ball and with four goals from five league outings, he appears a strong candidate for the buy of the season given that he was procured on a free transfer.</p>" +
				"<p>ST: Lionel Messi (Barcelona)</p>" +
				"<p>Messi was frustrated by the heavy-handed tactics employed by Celtic in the Champions League and was a markedly different player over the weekend. Given the license to roam free, he scored a brace and could have grabbed a hat-trick if his free-kick (from 35 yards!) or an attempt to chip the goalkeeper had managed to find the back of the net.</p>" +
				"<p>Worthy mention: Real�s Higuain was equally as impressive as Ronaldo in the win over Mallorca and has consolidated his position as a regular league starter.</p>" + 
				"<p>Kaka quandary: It�s still puzzling as to why Kaka did not get the nod for the starting XI for either of Real�s fixtures in the week. With the January transfer window looming ever closer, it wouldn�t be a surprise if the rumour mill begins to churn out reports of an impending loan move away from the club.</p>";
		
		tvHeadline.setText("Spanish La Liga Team of the Week");
		//ivImage.setImageDrawable(getResources().getDrawable(R.drawable.rmnewimage5));
		tvDate.setText("30-October-2012");
		wvContent.getSettings().setJavaScriptEnabled(true);
		wvContent.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
		wvContent
				.loadDataWithBaseURL(null, bodyStr, "text/html", "UTF-8", null);

	}
}