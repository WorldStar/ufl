package com.jmsb.ufl.news;

import com.jmsb.ufl.R;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;



public class NewsDetail2 extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.newsdetail);

		TextView tvHeadline = (TextView) findViewById(R.id.newsdetailheadline);
		//ImageView ivImage = (ImageView) findViewById(R.id.newsdetailimage);
		TextView tvDate = (TextView) findViewById(R.id.newsdetailpublishdate);
		WebView wvContent = (WebView) findViewById(R.id.newsdetailwebview);
		String bodyStr = "<p>It was a routine weekend for Barcelona and Real Madrid in La Liga, with both sides winning 5-0 away from home at Rayo Vallecano and Mallorca, respectively, while Atletico Madrid kept pace with the Catalans following their 3-1 win over Osasuna.</p>"+
							"<p>Elsewhere, Getafe upset Athletic Bilbao at San Mames, Betis beat Valencia and Levante comfortably overcame Granada at home. Goal.com looks at the pick of the performers from the weekend�s action in the Primera Division.</p>"+
							"<p>In goal, Miguel Angel Moya was a key man as his side produced a shock 2-1 win at Athletic Bilbao. His opposite number, Gorka Iraizoz, was also sensational.</p>"+
							"<p>In defence, Mario is included again following his fine performance in helping Betis to a clean sheet and an excellent 1-0 win at home to Valencia. Miranda was on form for Atletico in their latest success and also headed home one of the goals, while Martin Montoya was impressive for Barcelona in their stroll at Rayo, setting up the first of Lionel Messi�s two strikes.</p>"+
							"<p>Cristian Rodriguez was a constant threat for Los Rojiblancos against Osasuna and makes the midfield, along with the in-form Cesc Fabregas, who continues to rack up goals and assists in 2012-13 following an indifferent debut season last term. The other berth goes toJuan Rodriguez, who opened the scoring for Getafe at Athletic Bilbao and was excellent as the Madrid-based side claimed all three points.</p>"+
							"<p>Messi makes it into the side once again following his two great goals in Vallecas, while Levante�s Obafemi Martins is also included after hitting a double in the 3-1 win over Granada. Cristiano Ronaldo and Gonzalo Higuain grabbed two goals apiece, too, meaning eight goals were shared out between this week�s front four.</p>";		
		tvHeadline.setText("La Liga Team of the Week: Messi & Ronaldo joined by Fabregas & Higuain");
		//ivImage.setImageDrawable(getResources().getDrawable(R.drawable.rmnewimage2));
		tvDate.setText("30-October-2012");
		wvContent.getSettings().setJavaScriptEnabled(true);
		wvContent.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
		wvContent
				.loadDataWithBaseURL(null, bodyStr, "text/html", "UTF-8", null);

	}
}