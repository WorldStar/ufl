package com.jmsb.ufl.news;

import android.content.Intent;
import android.os.Bundle;

import com.jmsb.ufl.library.TabGroupActivity;

public class NewsTabGroup extends TabGroupActivity{
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		startChildActivity("NewsActivity", new Intent(this, News.class));
	}
}