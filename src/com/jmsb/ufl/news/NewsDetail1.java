package com.jmsb.ufl.news;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.TextView;
import com.jmsb.ufl.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class NewsDetail1 extends Activity {
	
	DisplayImageOptions options;
	BaseActivity base;
	protected ImageLoader imageLoader = ImageLoader.getInstance();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.newsdetail);
		
		/*
		options = new DisplayImageOptions.Builder()
		.showStubImage(R.drawable.ic_stub)
		.showImageForEmptyUri(R.drawable.ic_empty)
		.showImageOnFail(R.drawable.ic_error)
		.cacheInMemory()
		.cacheOnDisc()
		.displayer(new RoundedBitmapDisplayer(20))
		.build();
		
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
		 .defaultDisplayImageOptions(options)
		 .build();
		
		ImageLoader.getInstance().init(config);
		*/

		TextView tvHeadline = (TextView) findViewById(R.id.newsdetailheadline);
		//ImageView ivImage = (ImageView) findViewById(R.id.newsdetailimage);
		TextView tvDate = (TextView) findViewById(R.id.newsdetailpublishdate);
		WebView wvContent = (WebView) findViewById(R.id.newsdetailwebview);		
		
		Intent intent = getIntent();
		Bundle b = intent.getExtras();
		String headLine = b.getString("title");
		String date = b.getString("date");
        //String imageRecd = b.getString("image");
		String content = b.getString("content");
		
		tvHeadline.setText(headLine);
		//imageLoader.displayImage(imageRecd, ivImage);
		//ivImage.setVisibility(View.INVISIBLE);
		tvDate.setText(date);
		wvContent.getSettings().setJavaScriptEnabled(true);
		wvContent.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
		wvContent.loadDataWithBaseURL(null, content, "text/html", "UTF-8", null);
		
	}

}