package com.jmsb.ufl.result;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.*;
import android.widget.*;
import com.jmsb.ufl.R;
import com.jmsb.ufl.library.DBHelper;
import com.jmsb.ufl.library.ReturnMessage;
import com.jmsb.ufl.schedule.ScheduleObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Results extends Activity{
    private LayoutInflater mInflater;
	/*private Vector<RowData> data;
	RowData rd;
	static final String[] matchDate = new String[] { "3-OCT-2012",
		"7-OCT-2012", "21-OCT-2012", "24-OCT-2012", "31-OCT-2012" };

	static final String[] matchTime = new String[] { "18:45",
		"19:00", "14:00", "15:00", "14:00"};

	static final String[] localTeam = new String[] { "Man United", "Barcelona",
		"R Madrid", "R Betis", "Mallorca" };

	static final String[] visitorTeam = new String[] { "R Madrid", "R Madrid",
		"Valencia", "R Madrid", "R Madrid" };

	static final String[] localScore = new String[] { "1", "2", "1", "1", "0"};

	static final String[] visitorScore = new String[] { "2", "0", "1", "1", "0"};

	private Integer[] localImage = { R.drawable.manutd, R.drawable.bercelona,
			R.drawable.real_madrid, R.drawable.real_betis,
			R.drawable.mallorca };

	private Integer[] visitorImage = { R.drawable.real_madrid, R.drawable.real_madrid,
			R.drawable.valencia, R.drawable.real_madrid, R.drawable.real_madrid }; */
    boolean connctionflg = false;
    SharedPreferences mySP;
    SharedPreferences.Editor mySPEditor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listview);
        mySP = this.getSharedPreferences("myPref", MODE_PRIVATE);
        mySPEditor = mySP.edit();
        Typeface tf = Typeface.createFromAsset(getAssets(), "font/Helvetica.otf");
        TextView header = (TextView)findViewById(R.id.header);
        header.setText(getString(R.string.HeaderTitle_Results));
        //header.setTypeface(tf, Typeface.BOLD);
        new LoadSchedule(getContext(), false).execute();
		/*ListView lvSchedule = (ListView) findViewById(R.id.listview);

		mInflater = (LayoutInflater) getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		data = new Vector<RowData>();

		for (int i = 0; i < localTeam.length; i++) {
			try {
				rd = new RowData(i, localTeam[i]);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			data.add(rd);
		}
		CustomAdapter adapter = new CustomAdapter(this,
				R.layout.schedule_list_row, R.id.title, data);
		lvSchedule.setAdapter(adapter);
		lvSchedule.setTextFilterEnabled(true); */

		/*lvSchedule.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub

					switch(position){
					case 0:
						 Intent intent1 = new Intent(getParent(), Result1.class);
						 intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						 TabGroupActivity parentActivity1 = (TabGroupActivity) getParent();
						 parentActivity1.startChildActivity("Result1", intent1);
						 break;
					case 1:
						 Intent intent2 = new Intent(getParent(), Result2.class);
						 intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						 TabGroupActivity parentActivity2 = (TabGroupActivity) getParent();
						 parentActivity2.startChildActivity("Result2", intent2);
						 break;
					case 2:
						 Intent intent3 = new Intent(getParent(), Result3.class);
						 intent3.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						 TabGroupActivity parentActivity3 = (TabGroupActivity) getParent();
						 parentActivity3.startChildActivity("Result3", intent3);
						 break;
					case 3:
						 Intent intent4 = new Intent(getParent(), Result4.class);
						 intent4.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						 TabGroupActivity parentActivity4 = (TabGroupActivity) getParent();
						 parentActivity4.startChildActivity("Result4", intent4);
						 break;
					case 4:
						 Intent intent5 = new Intent(getParent(), Result5.class);
						 intent5.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						 TabGroupActivity parentActivity5 = (TabGroupActivity) getParent();
						 parentActivity5.startChildActivity("Result5", intent5);
						 break;

					}
			}
		}); */
    }
    private Context getContext() {
        Context context;
        if (getParent() != null) context = getParent();
        else context = this;
        return context;
    }

    public class LoadSchedule extends AsyncTask<Void, Void, List<ScheduleObject>> {
        Context context;
        ProgressDialog progressDialog;
        boolean isRefresh;

        public LoadSchedule(Context context, boolean isRefresh){
            super();
            this.context = context;
            this.isRefresh = isRefresh;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setTitle(getString(R.string.LoadingDialog_Title));
            progressDialog.setMessage(getString(R.string.LoadingDialog_Content1));
            progressDialog.show();
        }

        @Override
        protected List<ScheduleObject> doInBackground(Void... voids) {
            /*NodeList teamNodeList = getContentFromXML(getString(R.string.ScheduleUrl), "match");
            if(teamNodeList != null){
                List<ScheduleObject> scheduleList = convertNodeToObjectList(teamNodeList);
                return scheduleList;
            }
            return null;*/
            DBHelper db = new DBHelper(context);
            if(isRefresh){
                loadSchedule(db);
                return db.getSchedule(true);
            }else
                return db.getSchedule(true);
        }

        @Override
        protected void onPostExecute(List<ScheduleObject> list) {
            super.onPostExecute(list);
            progressDialog.dismiss();
            if(list != null){
                //list = resetScheduleList(list);
                showSchedule(context, list);
            }else{
                //ReturnMessage.showAlertDialog(context, "Unable to load", "Unable to load Schedule, please try again later.");
            	AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("Unable to load").setCancelable(false);
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                    	dialog.dismiss();
                    }
                });
                builder.setPositiveButton(getString(R.string.InternetNotFound_DialogPositiveButton), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        new LoadSchedule(context,false).execute();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        }

        private List<ScheduleObject> loadSchedule(DBHelper db) {
            /*NodeList teamNodeList = getContentFromXML(getString(R.string.ScheduleUrl), "match");
            if(teamNodeList != null){
                db.dropScheduleTable();
                return convertNodeToObjectList(teamNodeList, db);
            }*/
            NodeList teamNodeList = getContentFromXML(getString(R.string.ScheduleUrl), "match");
            if(teamNodeList != null){
                db.dropScheduleTable();
                List<ScheduleObject> scheduleList;
                scheduleList = convertNodeToObjectList(teamNodeList, db);
                //mySPEditor.putInt("match day",getMatchDay(scheduleList)).commit();
                return scheduleList;
            }
            else
                return null;
        }

        private NodeList getContentFromXML(String urlString, String tagName) {
            NodeList nodeList = null;
            try {
                URL url = new URL(urlString);
                URLConnection con = url.openConnection();
                con.setReadTimeout(30000);
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                Document doc = db.parse(con.getInputStream());
                doc.getDocumentElement().normalize();
                nodeList = doc.getElementsByTagName(tagName);
            }catch (SocketTimeoutException ex){
            	nodeList = null;
            	connctionflg = false;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return nodeList;
        }

        private List<ScheduleObject> convertNodeToObjectList(NodeList nodeList, DBHelper db) {
            Node node = null;
            List<ScheduleObject> scheduleList= new ArrayList<ScheduleObject>();
            ScheduleObject schedule;
            String matchDate = "";
            int position = 0;
            int nodeListSize = nodeList.getLength();
            for(int i=0; i<nodeListSize; i++){
                node = nodeList.item(i);
                schedule = new ScheduleObject();
                schedule.setHomeTeamAndAwayTeam(((Element) node).getAttribute("title"));
                schedule.setHomeScoreAndAwayScore(((Element) node).getAttribute("score"));
                schedule.setLocation(((Element) node).getAttribute("location"));
                schedule.setDateAndTime(((Element)node).getAttribute("date"));
                schedule.setDateAndTime1(((Element)node).getAttribute("time"));
                schedule.setHomeAndAwayTeamID(((Element)node).getAttribute("ids"));
                schedule.setMatchDay(Integer.parseInt(((Element)node).getAttribute("match_day")));
                if(matchDate.equalsIgnoreCase("") || !(matchDate.equalsIgnoreCase(schedule.getDate()))){
                    position = 0;
                    schedule.setMatchNumber(position);
                }else{
                    position = position +1;
                    schedule.setMatchNumber(position);
                }
                matchDate = schedule.getDate();
                scheduleList.add(schedule);
                db.setSchedule(schedule);
            }
            return scheduleList;
        }

        private void showSchedule(Context context, List<ScheduleObject> list) {
            //int match_day = getMatchDay(list);
            List<ScheduleObject> resetList = new ArrayList<ScheduleObject>();
            list = sortList(list);
            resetList = filterResult(list);
            if(resetList!=null){
                ListView scheduleListView = (ListView)findViewById(R.id.listview);
                ScheduleAdapter scheduleAdapter = new ScheduleAdapter(context, 0, resetList){
                    public boolean isEnabled(int position)
                    {
                        return false;
                    }
                };
                scheduleListView.setAdapter(scheduleAdapter);
            }
        }

        private List<ScheduleObject> filterResult(List<ScheduleObject> list) {
            List<ScheduleObject> resetList = new ArrayList<ScheduleObject>();
            ScheduleObject scheduleObject;
            int position = 0;
            String months;
            String match_date = "";
            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH) + 1;
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");
            try {
                //Date todayDate = simpleDateFormat.parse(year+"-"+month+"-"+day);
                //Date matchDate;
                String todayDate = ""+year+"-"+month+"-"+day;
                Date today = simpleDateFormat.parse(todayDate);
                Date matchDate;
                for(int i=0;i<list.size();i++){
                    scheduleObject = list.get(i);
                    matchDate = simpleDateFormat.parse(scheduleObject.getDate());
                    if(matchDate.before(today)||matchDate.equals(todayDate)){
                        if(match_date.length()==0 || match_date.equalsIgnoreCase(scheduleObject.getDate())){
                            scheduleObject.setMatchNumber(position);
                            position = position+1;
                        }else{
                            position = 0;
                            scheduleObject.setMatchNumber(position);
                            position = position+1;
                        }
                        match_date = scheduleObject.getDate();
                        scheduleObject.setdate(sdf.format(matchDate));
                        resetList.add(scheduleObject);
                    }
                }
            } catch (ParseException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            return resetList;
        }

        private List<ScheduleObject> sortList(List<ScheduleObject> list) {
            int current_position = 0;
            int chop_size;
            int list_size = list.size();
            ScheduleObject scheduleObject;
            List<ScheduleObject> scheduleObjectList = new ArrayList<ScheduleObject>();
            for(int i=0;i<list_size;i++){
                current_position = i;
                scheduleObject = list.get(i);
                chop_size = scheduleObject.getMatchNumber();
                for(int j=chop_size;j>=0;j--){
                    if((j+current_position)<list_size) {
                        scheduleObject = list.get(j+current_position);
                        scheduleObjectList.add(scheduleObject);
                    }
                }
                i = i + chop_size;
            }
            return scheduleObjectList;
        }

        private int getMatchDay(List<ScheduleObject> list) {
            int match_day = 1;
            ScheduleObject tempScheduleObject;
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH)+1;
            int day = c.get(Calendar.DAY_OF_MONTH);
            try{
                Date date_todayDate = new SimpleDateFormat("yyyy-MM-dd").parse(year+"-"+month+"-"+day);
                for(int i=0;i<list.size();i++){
                    tempScheduleObject = list.get(i);
                    String string_matchDate = tempScheduleObject.getDate();
                    Date date_matchDate = new SimpleDateFormat("yyyy-MM-dd").parse(string_matchDate);
                    if(date_matchDate.before(date_todayDate) || date_matchDate.equals(date_todayDate)){
                        match_day = tempScheduleObject.getMatchDay();
                        break;
                    }
                }
            } catch (ParseException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            return match_day;
        }
    }

    private class ScheduleAdapter extends ArrayAdapter<ScheduleObject> {
        Context context;
        List<ScheduleObject> list;
        String matchDate = "";
        int match_day;
        int last_number;

        public class ScheduleHolder{
            RelativeLayout dateBanner;
            RelativeLayout rowContent;
            TextView date;
            TextView time;
            TextView homeTeam;
            TextView homeScore;
            TextView awayTeam;
            TextView awayScore;
            ImageView homeLogo;
            ImageView awayLogo;
        }

        public ScheduleAdapter(Context context, int match_day, List<ScheduleObject> list) {
            super(context, match_day, list);
            this.context = context;
            this.list = list;
            this.match_day = match_day;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ScheduleHolder holder = null;
            Typeface tf = Typeface.createFromAsset(getAssets(), "font/Helvetica.otf");
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            if(convertView == null){
                convertView = inflater.inflate(R.layout.schedule_list_row, parent, false);
                holder = new ScheduleHolder();
                holder.date = (TextView)convertView.findViewById(R.id.tvDate);
                //holder.date.setTypeface(tf, Typeface.BOLD);
                holder.dateBanner = (RelativeLayout)convertView.findViewById(R.id.rlDateBanner);
                holder.rowContent = (RelativeLayout)convertView.findViewById(R.id.row_content);
                holder.time = (TextView)convertView.findViewById(R.id.tvTime);
                //holder.time.setTypeface(tf);
                holder.homeTeam = (TextView)convertView.findViewById(R.id.tvLocal_team);
                //holder.homeTeam.setTypeface(tf);
                holder.homeScore = (TextView)convertView.findViewById(R.id.tvLocal_score);
                //holder.homeScore.setTypeface(tf);
                holder.awayTeam = (TextView)convertView.findViewById(R.id.tvVisitor_team);
                //holder.awayTeam.setTypeface(tf);
                holder.awayScore = (TextView)convertView.findViewById(R.id.tvVisitor_score);
                //holder.awayScore.setTypeface(tf);
                holder.homeLogo = (ImageView)convertView.findViewById(R.id.ivLocalImage);
                holder.awayLogo = (ImageView)convertView.findViewById(R.id.ivVisitorImage);
                convertView.setTag(holder);
            }
            else
                holder = (ScheduleHolder)convertView.getTag();
            ScheduleObject  scheduleObject = list.get(position);
            if(scheduleObject.getMatchNumber() != 0){
                holder.dateBanner.setVisibility(View.GONE);
            }else{
                holder.dateBanner.setVisibility(View.VISIBLE);
                holder.date.setText(scheduleObject.getDate());
            }
            holder.time.setText(scheduleObject.getTime());
            holder.homeTeam.setText(scheduleObject.getHomeTeam());
            holder.homeScore.setText(scheduleObject.getHomeScore());
            holder.awayTeam.setText(scheduleObject.getAwayTeam());
            holder.awayScore.setText(scheduleObject.getAwayScore());
            File imgFile = new File(Environment.getExternalStorageDirectory().toString()+"/TaiwanIntercity/"+scheduleObject.getHomeTeamID()+".png");
            if(imgFile.exists()){
                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                holder.homeLogo.setImageBitmap(myBitmap);
            }else{
            	imgFile=new File(getApplicationContext().getFilesDir().getAbsolutePath()+"/TaiwanIntercity/"+scheduleObject.getHomeTeamID()+".png");
            	if(imgFile.exists()){
                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    holder.homeLogo.setImageBitmap(myBitmap);
                }else{
                	holder.homeLogo.setImageResource(R.drawable.rihanna);
                }
            }
            imgFile = new File(Environment.getExternalStorageDirectory().toString()+"/TaiwanIntercity/"+scheduleObject.getAwayTeamID()+".png");
            if(imgFile.exists()){
                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                holder.awayLogo.setImageBitmap(myBitmap);
            }else{
            	imgFile=new File(getApplicationContext().getFilesDir().getAbsolutePath()+"/TaiwanIntercity/"+scheduleObject.getAwayTeamID()+".png");
            	if(imgFile.exists()){
                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    holder.awayLogo.setImageBitmap(myBitmap);
                }else{
                	holder.awayLogo.setImageResource(R.drawable.rihanna);
                }
            }
            matchDate = scheduleObject.getDate();
            return convertView;
        }
    }


    //hardcoded result
	/*private class RowData {
		protected int mId;
		protected String mTitle;

		RowData(int id, String title) {
			mId = id;
			mTitle = title;
		}

		@Override
		public String toString() {
			return mId + " " + mTitle;
		}
	}

	private class CustomAdapter extends ArrayAdapter<RowData> {
		public CustomAdapter(Context context, int resource,
				int textViewResourceId, List<RowData> objects) {
			super(context, resource, textViewResourceId, objects);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder = null;
			RelativeLayout rlDateBanner = null;
			TextView tvMatchDate = null;
			TextView tvMatchTime = null;
			TextView tvLocalScore = null;
			ImageView ivLocalImage = null;
			ImageView ivVisitorImage = null;
			TextView tvVisitorScore = null;
			TextView tvLocalTeam = null;
			TextView tvVisitorTeam = null;

			RowData rowData = getItem(position);
			if (null == convertView) {
				convertView = mInflater.inflate(R.layout.schedule_list_row,
						null);
				holder = new ViewHolder(convertView);
				convertView.setTag(holder);
			}
			holder = (ViewHolder) convertView.getTag();

			rlDateBanner = holder.getDateBanner();
			if (rowData.mId != 0) {
				if (matchDate[rowData.mId]
						.contentEquals(matchDate[rowData.mId - 1])) {
					rlDateBanner.setVisibility(View.GONE);
				}
			}

			tvMatchDate = holder.getMatchDate();
			tvMatchDate.setText(matchDate[rowData.mId]);

			tvMatchTime = holder.getMatchTime();
			tvMatchTime.setText(matchTime[rowData.mId]);

			tvLocalScore = holder.getLocalScore();
			tvLocalScore.setText(localScore[rowData.mId]);
			//tvLocalScore.setVisibility(View.INVISIBLE);

			ivLocalImage = holder.getLocalImage();
			ivLocalImage.setImageResource(localImage[rowData.mId]);

			ivVisitorImage = holder.getVisitorImage();
			ivVisitorImage.setImageResource(visitorImage[rowData.mId]);

			tvVisitorScore = holder.getVisitorScore();
			tvVisitorScore.setText(visitorScore[rowData.mId]);
			//tvVisitorScore.setVisibility(View.INVISIBLE);

			tvLocalTeam = holder.getLocalTeam();
			tvLocalTeam.setText(localTeam[rowData.mId]);

			tvVisitorTeam = holder.getVisitorTeam();
			tvVisitorTeam.setText(visitorTeam[rowData.mId]);
			return convertView;
		}

		private class ViewHolder {
			private View mRow;
			private RelativeLayout rlDateBanner;
			private TextView tvMatchDate;
			private TextView tvMatchTime;
			private TextView tvLocalScore;
			private ImageView ivLocalImage;
			private ImageView ivVisitorImage;
			private TextView tvVisitorScore;
			private TextView tvLocalTeam;
			private TextView tvVisitorTeam;

			public ViewHolder(View row) {
				mRow = row;
			}

			public RelativeLayout getDateBanner() {
				if (rlDateBanner == null) {
					rlDateBanner = (RelativeLayout) mRow
							.findViewById(R.id.rlDateBanner);
				}
				return rlDateBanner;
			}

			public TextView getMatchDate() {
				if (tvMatchDate == null) {
					tvMatchDate = (TextView) mRow.findViewById(R.id.tvDate);
				}
				return tvMatchDate;
			}

			public TextView getMatchTime() {
				if (tvMatchTime == null) {
					tvMatchTime = (TextView) mRow.findViewById(R.id.tvTime);
				}
				return tvMatchTime;
			}

			public TextView getLocalScore() {
				if (tvLocalScore == null) {
					tvLocalScore = (TextView) mRow
							.findViewById(R.id.tvLocal_score);
				}
				return tvLocalScore;
			}

			public ImageView getLocalImage() {
				if (ivLocalImage == null) {
					ivLocalImage = (ImageView) mRow
							.findViewById(R.id.ivLocalImage);
				}
				return ivLocalImage;
			}

			public ImageView getVisitorImage() {
				if (ivVisitorImage == null) {
					ivVisitorImage = (ImageView) mRow
							.findViewById(R.id.ivVisitorImage);
				}
				return ivVisitorImage;
			}

			public TextView getVisitorScore() {
				if (tvVisitorScore == null) {
					tvVisitorScore = (TextView) mRow
							.findViewById(R.id.tvVisitor_score);
				}
				return tvVisitorScore;
			}

			public TextView getLocalTeam() {
				if (tvLocalTeam == null) {
					tvLocalTeam = (TextView) mRow
							.findViewById(R.id.tvLocal_team);
				}
				return tvLocalTeam;
			}

			public TextView getVisitorTeam() {
				if (tvVisitorTeam == null) {
					tvVisitorTeam = (TextView) mRow
							.findViewById(R.id.tvVisitor_team);
				}
				return tvVisitorTeam;
			}
		}
	} */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.option_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_refresh:
                new LoadSchedule(getContext(), true).execute();
                break;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage(getString(R.string.ExitDialog_Content)).setCancelable(false);
        builder.setNegativeButton(getString(R.string.ExitDialog_NoButton), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.setPositiveButton(getString(R.string.ExitDialog_YesButton), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }
}